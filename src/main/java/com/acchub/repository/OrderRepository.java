package com.acchub.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.acchub.entity.OrderEnt;

public interface OrderRepository extends CrudRepository<OrderEnt, Long>{
	@Query(value = "SELECT * FROM orders WHERE userid = ?1", nativeQuery = true)
	Collection<OrderEnt> findByUserId(Long userid);
	
	@Query(value = "SELECT * FROM orders WHERE accountid = ?1", nativeQuery = true)
	Collection<OrderEnt> findByAccountId(Long accountid);
	
	@Query(value = "select * from orders where created_at BETWEEN DATE_SUB(NOW(), INTERVAL ?1 DAY) AND NOW() AND gameid = ?2", nativeQuery = true)
	Collection<OrderEnt> findByCustomDay(int day, Long gameid);
	
	@Query(value = "select * from orders where created_at >= ?1 AND created_at <= ?2 AND gameid = ?3", nativeQuery = true)
	Collection<OrderEnt> findByCustomDate(String date, String date1, Long gameid);
	
	@Query(value = "select * from orders where created_at BETWEEN DATE_SUB(NOW(), INTERVAL ?1 DAY) AND NOW()", nativeQuery = true)
	Collection<OrderEnt> findAllByCustomDay(int day);
	
	@Query(value = "select * from orders where created_at >= ?1 AND created_at <= ?2", nativeQuery = true)
	Collection<OrderEnt> findAllByCustomDate(String date, String date1);
	
	@Query(value = "select * from orders where created_at BETWEEN DATE_SUB(NOW(), INTERVAL ?1 DAY) AND NOW() AND userid = ?2", nativeQuery = true)
	Collection<OrderEnt> findAllByUserDay(int day, Long id);
	
	@Query(value = "select * from orders where created_at >= ?1 AND created_at <= ?2 AND userid = ?2", nativeQuery = true)
	Collection<OrderEnt> findAllByUserDate(String date, String date1, Long id);
}

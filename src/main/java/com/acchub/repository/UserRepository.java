package com.acchub.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.acchub.entity.UserEnt;

public interface UserRepository extends CrudRepository<UserEnt, Long>{
	@Query(value = "SELECT * FROM USERS WHERE username = ?1 and password = ?2", nativeQuery = true)
	UserEnt checkLogin(String username, String password);
	
	@Query(value = "SELECT id FROM USERS WHERE username = ?1", nativeQuery = true)
	UserEnt findByUsername(String username);
	
	@Query(value = "SELECT id FROM USERS WHERE email = ?1", nativeQuery = true)
	UserEnt findByEmail(String email);
	
	
	@Query(value = "SELECT id FROM users WHERE username = ?1", nativeQuery = true)
	Long countUsersWithUsername(String username);	
	
	@Query(value = "SELECT id FROM users WHERE email = ?1", nativeQuery = true)
	Long countUsersWithEmail(String email);	
}

package com.acchub.repository;

import org.springframework.data.repository.CrudRepository;

import com.acchub.entity.GameEnt;

public interface GameRepository extends CrudRepository<GameEnt, Long>{

}

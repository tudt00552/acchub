package com.acchub.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.acchub.entity.AccountEnt;

public interface AccIFRepository extends CrudRepository<AccountEnt, Long>{
	@Modifying
    @Transactional
	@Query(value = "update gameaccount set status = ?1 where id = ?2", nativeQuery = true)
	void updateStatus(int status, Long id);
	
	@Query(value = "SELECT id FROM gameaccount WHERE accountid = ?1", nativeQuery = true)
	Long checkExitsAccID(String accountid);	
	
	@Query(value = "SELECT id FROM gameaccount WHERE id = ?1", nativeQuery = true)
	Long checkNullAcc(Long id);	
	
	@Query(value = "SELECT * FROM gameaccount WHERE status = ?1", nativeQuery = true)
	Collection<AccountEnt> findByStatus(int status);

}

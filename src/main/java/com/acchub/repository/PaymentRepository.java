package com.acchub.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.acchub.entity.PaymentEnt;

public interface PaymentRepository extends CrudRepository<PaymentEnt, Long>{
	@Query(value = "SELECT * FROM payment WHERE userid = ?1", nativeQuery = true)
	Collection<PaymentEnt> findPaymentByID(Long userid);	
}

package com.acchub.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.acchub.entity.Account;

public interface AccRepository extends CrudRepository<Account, Long>{
		@Modifying
	    @Transactional
	 	@Query(value = "delete from account where accountid = ?1", nativeQuery = true)
		void deleteAccountByAccouniftid(Long id);
}

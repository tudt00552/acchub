package com.acchub.service;

import java.util.Collection;

import com.acchub.entity.GameEnt;

public interface GameServiceLocal {
	GameEnt save(GameEnt game);
	
	Collection<GameEnt> findAllGame();
	
	GameEnt findOne(long id);

	void delete(long id);
}

package com.acchub.service;

import java.util.Collection;

import com.acchub.entity.Account;

public interface AccServiceLocal {
	Account save(Account account);
	
	Collection<Account> findAllAccount();

	void delete(long id);

	void deletebyIfID(long id);
}

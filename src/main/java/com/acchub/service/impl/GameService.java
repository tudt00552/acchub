package com.acchub.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acchub.entity.GameEnt;
import com.acchub.repository.GameRepository;
import com.acchub.service.GameServiceLocal;

@Service
public class GameService implements GameServiceLocal{
	
	@Autowired
	private GameRepository gameRepository;
	
	@Override
	public GameEnt save(GameEnt game) {
		game = gameRepository.save(game);
		return game;
	}
	
	@Override
	public Collection<GameEnt> findAllGame() {
		List<GameEnt> accs = new ArrayList<GameEnt>(); 
		for(GameEnt acc : gameRepository.findAll()) {
			accs.add(acc);
		}
		return accs;
	}
	
	@Override
	public void delete(long id) {
		gameRepository.delete(id);
	}
	
	@Override
	public GameEnt findOne(long id) {
		return gameRepository.findOne(id);
	}
}

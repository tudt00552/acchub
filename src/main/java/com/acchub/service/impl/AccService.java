package com.acchub.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acchub.entity.Account;
import com.acchub.repository.AccRepository;
import com.acchub.service.AccServiceLocal;

@Service
public class AccService implements AccServiceLocal{
	@Autowired
	private AccRepository accRepository;
	
	@Override
	public Account save(Account account) {
		account.setStatus(1);
		account = accRepository.save(account);
		return account;
	}
	
	@Override
	public Collection<Account> findAllAccount() {
		List<Account> accs = new ArrayList<Account>(); 
		for(Account acc : accRepository.findAll()) {
			accs.add(acc);
		}
		return accs;
	}
	
	@Override
	public void delete(long id) {
		accRepository.delete(id);
	}
	
	@Override
	public void deletebyIfID(long id) {
		accRepository.deleteAccountByAccouniftid(id);
	}
	
}

package com.acchub.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acchub.entity.OrderEnt;
import com.acchub.repository.OrderRepository;
import com.acchub.service.OrderServiceLocal;

@Service
public class OrderService implements OrderServiceLocal{
	@Autowired
	private OrderRepository orderRepository;
	
	@Override
	public Collection<OrderEnt> findAllOrder() {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		for(OrderEnt order : orderRepository.findAll()) {
			orders.add(order);
		}
		return orders;
	}
	
	public OrderEnt save(OrderEnt orderEnt) {
		orderEnt = orderRepository.save(orderEnt);
		return orderEnt;
	}
	
	@Override
	public Collection<OrderEnt> findByUserId(Long userid) {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		for(OrderEnt order : orderRepository.findByUserId(userid)) {
			orders.add(order);
		}
		return orders;
	}

	@Override
	public Collection<OrderEnt> findByAccountId(Long accountid) {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		for(OrderEnt order : orderRepository.findByAccountId(accountid)) {
			orders.add(order);
		}
		return orders;
	}
	
	@Override
	public Collection<OrderEnt> findByCustomDay(int day, Long gameid) {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		for(OrderEnt order : orderRepository.findByCustomDay(day, gameid)) {
			orders.add(order);
		}
		return orders;
	}
	
	@Override
	public Collection<OrderEnt> findAllByCustomDay(int day) {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		for(OrderEnt order : orderRepository.findAllByCustomDay(day)) {
			orders.add(order);
		}
		return orders;
	}
	
	@Override
	public Collection<OrderEnt> findAllByCustomDate(String date, String date1) {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		String[] d = date1.split("-");
		int d2 = Integer.parseInt(d[2]) + 1;
		String d2f = d[0] + "-" + "-" + d[1] + "-" + d2;
		for(OrderEnt order : orderRepository.findAllByCustomDate(date, d2f)) {
			orders.add(order);
		}
		return orders;
	}
	
	@Override
	public Collection<OrderEnt> findAllByUserDay(int day, long id) {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		for(OrderEnt order : orderRepository.findAllByUserDay(day, id)) {
			orders.add(order);
		}
		return orders;
	}
	
	@Override
	public Collection<OrderEnt> findAllByUserDate(String date, String date1, long id) {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		String[] d = date1.split("-");
		int d2 = Integer.parseInt(d[2]) + 1;
		String d2f = d[0] + "-" + "-" + d[1] + "-" + d2;
		for(OrderEnt order : orderRepository.findAllByUserDate(date, d2f, id)) {
			orders.add(order);
		}
		return orders;
	}
	
	@Override
	public Collection<OrderEnt> findByCustomDate(String date, String date1, Long gameid) {
		List<OrderEnt> orders = new ArrayList<OrderEnt>(); 
		String[] d = date1.split("-");
		int d2 = Integer.parseInt(d[2]) + 1;
		String d2f = d[0] + "-" + "-" + d[1] + "-" + d2;
		for(OrderEnt order : orderRepository.findByCustomDate(date, d2f, gameid)) {
			orders.add(order);
		}
		return orders;
	}
}

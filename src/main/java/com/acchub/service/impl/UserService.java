package com.acchub.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acchub.entity.UserEnt;
import com.acchub.repository.UserRepository;
import com.acchub.service.UserServiceLocal;

@Service
public class UserService implements UserServiceLocal{
	
	@Autowired
	private UserRepository acchubRepository;
	
	@Override
	public UserEnt save(UserEnt user) {
		user = acchubRepository.save(user);
		return user;
	}
	
	@Override
	public UserEnt checkLogin(String username, String password) {
		UserEnt userEnt = new UserEnt();
		userEnt = acchubRepository.checkLogin(username, password);
		return userEnt;
	}

	@Override
	public Collection<UserEnt> findAllUser() {
		List<UserEnt> users = new ArrayList<UserEnt>(); 
		for(UserEnt user : acchubRepository.findAll()) {
			users.add(user);
		}
		return users;
	}
	
	@Override
	public Long checkExitsUsername(String username)
	{
		return acchubRepository.countUsersWithUsername(username);
	}
	
	@Override
	public Long checkExitsEmail(String email)
	{
		return acchubRepository.countUsersWithEmail(email);
	}
	
	
	@Override
	public UserEnt findOne(long id) {
		return acchubRepository.findOne(id);
	}
	
	@Override
	public UserEnt findByUsername(String username) {
		return acchubRepository.findByUsername(username);
	}
	
	@Override
	public UserEnt findByEmail(String email) {
		return acchubRepository.findByUsername(email);
	}

}

package com.acchub.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acchub.entity.OrderEnt;
import com.acchub.entity.PaymentEnt;
import com.acchub.entity.UserEnt;
import com.acchub.repository.PaymentRepository;
import com.acchub.service.PaymentServiceLocal;

@Service
public class PaymentService implements PaymentServiceLocal{
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Override
	public PaymentEnt save(PaymentEnt paymentEnt) {
		paymentEnt = paymentRepository.save(paymentEnt);
		return paymentEnt;
	}
	
	@Override
	public Collection<PaymentEnt> findPaymentByID(Long userid) {
		List<PaymentEnt> payments = new ArrayList<PaymentEnt>(); 
		for(PaymentEnt payment : paymentRepository.findPaymentByID(userid)) {
			payments.add(payment);
		}
		return payments;
	}
	
	@Override
	public Collection<PaymentEnt> findAllPayment() {
		List<PaymentEnt> payins = new ArrayList<PaymentEnt>(); 
		for(PaymentEnt pay : paymentRepository.findAll()) {
			payins.add(pay);
		}
		return payins;
	}
}

package com.acchub.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acchub.entity.AccountEnt;
import com.acchub.entity.OrderEnt;
import com.acchub.repository.AccIFRepository;
import com.acchub.service.AccIFServiceLocal;

@Service
public class AccIFService implements AccIFServiceLocal{
	
	@Autowired
	private AccIFRepository accIFRepository;
	
	@Override
	public AccountEnt save(AccountEnt accountEnt) {
		accountEnt.setStatus(1);
		accountEnt = accIFRepository.save(accountEnt);
		return accountEnt;
	}
	
	@Override
	public Collection<AccountEnt> findAllAcc() {
		List<AccountEnt> accs = new ArrayList<AccountEnt>(); 
		for(AccountEnt acc : accIFRepository.findAll()) {
			accs.add(acc);
		}
		return accs;
	}
	
	@Override
	public Collection<AccountEnt> findByStatus(int status) {
		List<AccountEnt> accs = new ArrayList<AccountEnt>(); 
		for(AccountEnt acc : accIFRepository.findByStatus(status)) {
			accs.add(acc);
		}
		return accs;
	}
	
	@Override
	public void delete(long id) {
		accIFRepository.delete(id);
	}
	
	@Override
	public void updateStatus(int status, long id) {
		accIFRepository.updateStatus(status, id);
	}
	
	@Override
	public Long checkExitsAccID(String accountid)
	{
		return accIFRepository.checkExitsAccID(accountid);
	}
	
	@Override
	public Long checkNullAcc(Long id)
	{
		return accIFRepository.checkNullAcc(id);
	}
	
	@Override
	public AccountEnt findOne(long id) {
		return accIFRepository.findOne(id);
	}
}

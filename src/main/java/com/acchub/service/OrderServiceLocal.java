package com.acchub.service;

import java.util.Collection;

import com.acchub.entity.OrderEnt;

public interface OrderServiceLocal {

	Collection<OrderEnt> findAllOrder();
	
	OrderEnt save(OrderEnt orderEnt);
	
	Collection<OrderEnt> findByUserId(Long userid);

	Collection<OrderEnt> findByCustomDay(int day, Long gameid);

	Collection<OrderEnt> findByCustomDate(String date, String date1, Long gameid);

	Collection<OrderEnt> findAllByCustomDate(String date, String date1);

	Collection<OrderEnt> findAllByCustomDay(int day);

	Collection<OrderEnt> findAllByUserDate(String date, String date1, long id);

	Collection<OrderEnt> findAllByUserDay(int day, long id);

	Collection<OrderEnt> findByAccountId(Long accountid);
}

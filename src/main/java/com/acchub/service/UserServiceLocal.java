package com.acchub.service;

import java.util.Collection;

import com.acchub.entity.UserEnt;

public interface UserServiceLocal {
	UserEnt save(UserEnt user);
	
	Collection<UserEnt> findAllUser();
	
	UserEnt findOne(long id);
	
	UserEnt checkLogin(String username, String password);
	
	UserEnt findByUsername(String username);
	
	UserEnt findByEmail(String email);

	Long checkExitsUsername(String username);

	Long checkExitsEmail(String email);
}

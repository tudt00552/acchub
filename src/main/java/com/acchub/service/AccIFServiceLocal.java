package com.acchub.service;

import java.util.Collection;

import com.acchub.entity.AccountEnt;

public interface AccIFServiceLocal {
	AccountEnt save(AccountEnt accountEnt);
	
	Collection<AccountEnt> findAllAcc();
	
	AccountEnt findOne(long id);

	void delete(long id);

	void updateStatus(int status, long id);

	Long checkExitsAccID(String accountid);

	Collection<AccountEnt> findByStatus(int status);

	Long checkNullAcc(Long id);
}

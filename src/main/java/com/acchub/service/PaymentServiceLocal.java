package com.acchub.service;

import java.util.Collection;

import com.acchub.entity.PaymentEnt;

public interface PaymentServiceLocal {

	PaymentEnt save(PaymentEnt paymentEnt);

	Collection<PaymentEnt> findPaymentByID(Long userid);

	Collection<PaymentEnt> findAllPayment();

}

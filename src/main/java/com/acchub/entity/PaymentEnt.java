package com.acchub.entity;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "payment")
@EntityListeners(AuditingEntityListener.class)
public class PaymentEnt extends BaseEntity{
	 @Column
	 private String payid;
	 
	 @Column
	 private String payername;
	 
	 @Column
	 private String payemail;
	 
	 @Column
	 private double usd;
	 
	 @Column
	 private BigInteger vnd;
	 
	 @Column
	 private int status;
	 
	 public PaymentEnt() {
		 
	 }
	 
	 public PaymentEnt(String payid, String payername, String payemail, double usd, BigInteger vnd, int status,
			UserEnt userp) {
		super();
		this.payid = payid;
		this.payername = payername;
		this.payemail = payemail;
		this.usd = usd;
		this.vnd = vnd;
		this.status = status;
		this.userp = userp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid", referencedColumnName = "id")
    private UserEnt userp;

	public String getPayid() {
		return payid;
	}

	public void setPayid(String payid) {
		this.payid = payid;
	}

	public String getPayername() {
		return payername;
	}

	public void setPayername(String payername) {
		this.payername = payername;
	}

	public String getPayemail() {
		return payemail;
	}

	public void setPayemail(String payemail) {
		this.payemail = payemail;
	}

	public double getUsd() {
		return usd;
	}

	public void setUsd(double usd) {
		this.usd = usd;
	}

	public BigInteger getVnd() {
		return vnd;
	}

	public void setVnd(BigInteger vnd) {
		this.vnd = vnd;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public UserEnt getUserp() {
		return userp;
	}

	public void setUserp(UserEnt userp) {
		this.userp = userp;
	}
	 
	 
}

package com.acchub.entity;

import java.math.BigInteger;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
public class UserEnt extends BaseEntity {

	@Column
	private int role;

	@Column
	private String username;

	@Column
	private String password;

	@Column
	private String email;

	@Column
	private String fullname;

	@Column
	private BigInteger balance;
	
	@Column(name = "status")
	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Collection<OrderEnt> orderlist;
	
	@OneToMany(mappedBy = "userp", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Collection<PaymentEnt> paylist;

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public BigInteger getBalance() {
		return balance;
	}

	public void setBalance(BigInteger balance) {
		this.balance = balance;
	}

	public Collection<OrderEnt> getOrderlist() {
		return orderlist;
	}

	public void setOrderlist(Collection<OrderEnt> orderlist) {
		this.orderlist = orderlist;
	}
}

package com.acchub.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "account")
@EntityListeners(AuditingEntityListener.class)
public class Account extends BaseEntity{
	@Column
	private String username;
	
	@Column
	private String password;
	
	@Column(name = "status")
	private int status;
	
	@OneToOne
    @JoinColumn(name = "accountid", referencedColumnName = "id")
    private AccountEnt accountEnt;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public AccountEnt getAccountEnt() {
		return accountEnt;
	}

	public void setAccountEnt(AccountEnt accountEnt) {
		this.accountEnt = accountEnt;
	}
	
}

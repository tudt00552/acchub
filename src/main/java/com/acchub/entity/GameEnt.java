	package com.acchub.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "gamedetails")
@EntityListeners(AuditingEntityListener.class)
public class GameEnt extends BaseEntity{
	
	@Column
	private String title;
	
	@Column
	private String image;
	
	@Column
	private String description;
	
	@Column(name = "status")
	private int status;
	
	@OneToMany(mappedBy = "game", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Collection<OrderEnt> orderlist;

	public Collection<OrderEnt> getOrderlist() {
		return orderlist;
	}

	public void setOrderlist(Collection<OrderEnt> orderlist) {
		this.orderlist = orderlist;
	}

	@OneToMany(mappedBy = "gameEnt", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<AccountEnt> accountEnts;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<AccountEnt> getAccountEnts() {
		return accountEnts;
	}

	public void setAccountEnts(Collection<AccountEnt> accountEnts) {
		this.accountEnts = accountEnts;
	}

}

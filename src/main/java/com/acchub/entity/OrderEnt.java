package com.acchub.entity;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "orders")
@EntityListeners(AuditingEntityListener.class)
public class OrderEnt extends BaseEntity{
	
	@Column
	private String ordercode;
	
	private BigInteger total;
	
	public BigInteger getTotal() {
		return total;
	}

	public void setTotal(BigInteger total) {
		this.total = total;
	}

	@Column(name = "status")
	private int status;

	public String getOrdercode() {
		return ordercode;
	}

	public void setOrdercode(String ordercode) {
		this.ordercode = ordercode;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public UserEnt getUser() {
		return user;
	}

	public void setUser(UserEnt user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid", referencedColumnName = "id")
    private UserEnt user;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gameid", referencedColumnName = "id")
    private GameEnt game;

	public GameEnt getGame() {
		return game;
	}

	public void setGame(GameEnt game) {
		this.game = game;
	}

	@OneToOne
    @JoinColumn(name = "accountid", referencedColumnName = "id")
    private AccountEnt accountEnts;

	public AccountEnt getAccountEnts() {
		return accountEnts;
	}

	public void setAccountEnts(AccountEnt accountEnts) {
		this.accountEnts = accountEnts;
	}
	
	
}

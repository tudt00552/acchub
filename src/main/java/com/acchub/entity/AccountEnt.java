package com.acchub.entity;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "gameaccount")
@EntityListeners(AuditingEntityListener.class)
public class AccountEnt extends BaseEntity{
	
	@Column
	private String accountid;
	
	@Column
	private String accountinfo;
	
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	@Column(length=1000)
	private String image;
	
	@Column
	private String cover;

	@Column
	private BigInteger price;
	
	@Column(name = "status")
	private int status;
	
	@ManyToOne 
    @JoinColumn(name = "gameid", referencedColumnName = "id")
    private GameEnt gameEnt;
	
	@OneToOne(mappedBy = "accountEnt")
    private Account account;
	
	@OneToOne(mappedBy = "accountEnts")
    private OrderEnt orderEnt;

	public int getStatus() {
		return status;
	}

	public OrderEnt getOrderEnt() {
		return orderEnt;
	}

	public void setOrderEnt(OrderEnt orderEnt) {
		this.orderEnt = orderEnt;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAccountid() {
		return accountid;
	}
	
	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public String getAccountinfo() {
		return accountinfo;
	}

	public void setAccountinfo(String accountinfo) {
		this.accountinfo = accountinfo;
	}

	public BigInteger getPrice() {
		return price;
	}

	public void setPrice(BigInteger price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setGameEnt(GameEnt gameEnt) {
		this.gameEnt = gameEnt;
	}

	public GameEnt getGameEnt() {
		return gameEnt;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
}

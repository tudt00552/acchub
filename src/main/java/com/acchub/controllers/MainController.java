package com.acchub.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.acchub.entity.Account;
import com.acchub.entity.AccountEnt;
import com.acchub.entity.GameEnt;
import com.acchub.entity.OrderEnt;
import com.acchub.entity.PaymentEnt;
import com.acchub.entity.UserEnt;
import com.acchub.payhub.PaypalPaymentIntent;
import com.acchub.payhub.PaypalPaymentMethod;
import com.acchub.payhub.PaypalService;
import com.acchub.payhub.Utils;
import com.acchub.service.AccIFServiceLocal;
import com.acchub.service.AccServiceLocal;
import com.acchub.service.GameServiceLocal;
import com.acchub.service.OrderServiceLocal;
import com.acchub.service.PaymentServiceLocal;
import com.acchub.service.UserServiceLocal;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;;

@Controller
public class MainController {
	
	/* ----------------------------USER---------------------------- */

	@Autowired
	private UserServiceLocal userService;
	
	@Autowired
	private GameServiceLocal gameService;
	
	@Autowired
	private AccIFServiceLocal accIFServiceLocal;
	
	@Autowired
	private AccServiceLocal accServiceLocal;
	
	@Autowired
	private OrderServiceLocal orderServiceLocal;
	
	@Autowired
	private PaypalService paypalService;
	
	@Autowired
	private PaymentServiceLocal paymentServiceLocal;
	
	public static final String URL_PAYPAL_SUCCESS = "success";
	public static final String URL_PAYPAL_CANCEL = "cancel";
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@GetMapping("/")
	public String init(HttpServletRequest req) {
		HttpSession session = req.getSession();
		Collection<GameEnt> gamelist = gameService.findAllGame();
	
		req.setAttribute("gameacc", gamelist);
		if (session.getAttribute("userid") == null || session.getAttribute("userid").equals("")) {
			return "index";
		} else {
			Long userid = (Long) session.getAttribute("userid");
			req.setAttribute("users", userService.findOne(userid));
			return "index";
		}
	}
	
	@GetMapping("/admin")
	public String admin(HttpServletRequest req) {
		req.setAttribute("MODE", "HOME");
		req.setAttribute("user", userService.findAllUser());
		req.setAttribute("game", gameService.findAllGame());
		req.setAttribute("gameacc", accIFServiceLocal.findAllAcc());
		req.setAttribute("order", orderServiceLocal.findAllOrder());
		req.setAttribute("payment", paymentServiceLocal.findAllPayment());
		List<AccountEnt> accountlist = new ArrayList<AccountEnt>(accIFServiceLocal.findAllAcc());
		List<AccountEnt> soldacc = new ArrayList<AccountEnt>();	
		List<AccountEnt> activeacc = new ArrayList<AccountEnt>();
		for (AccountEnt accountEnt : accountlist) {
			if (accountEnt.getStatus() == 2) {
				soldacc.add(accountEnt);
			}
			else if (accountEnt.getStatus() == 1){
				activeacc.add(accountEnt);
			}
		}
		BigInteger sum = BigInteger.ZERO;
		for (AccountEnt accountEnt : accIFServiceLocal.findByStatus(2)) {
			sum = sum.add(accountEnt.getPrice());
		}
		BigInteger sum1 = BigInteger.ZERO;
		for (PaymentEnt paymentEnt : paymentServiceLocal.findAllPayment()) {
			sum1 = sum1.add(paymentEnt.getVnd());
		}
		req.setAttribute("sum", sum);
		req.setAttribute("sum1", sum1);
		req.setAttribute("accountlist", accountlist);
		req.setAttribute("activeacc", activeacc);
		req.setAttribute("soldacc", soldacc);
//		HttpSession session = req.getSession();
//		if (session.getAttribute("userid") == null || session.getAttribute("userid").equals("")) {
//			return "index";
//		} else {
//			Long userid = (Long) session.getAttribute("userid");
//			req.setAttribute("users", userService.findOne(userid));
//			if(userService.findOne(userid).getRole() == 1) {
//				Collection<GameEnt> gamelist = gameService.findAllGame();
//				req.setAttribute("gameacc", gamelist);
//				return "index";
//			}
//			else {
//				return "admin";
//			}
//		}
		return "admin";
	}

	@GetMapping("/logout")
	public void logout(HttpServletRequest req, HttpServletResponse res) throws IOException {
		HttpSession session = req.getSession(false);
		session.removeAttribute("userid");
		res.sendRedirect("/");
	}

	@GetMapping("/account")
	public String account(@RequestParam long id, HttpServletRequest req) {
		HttpSession session = req.getSession();
		GameEnt game = gameService.findOne(id);
		req.setAttribute("game", game);
		if (session.getAttribute("userid") == null || session.getAttribute("userid").equals("")) {
			return "account";
		} else {
			Long userid = (Long) session.getAttribute("userid");
			req.setAttribute("users", userService.findOne(userid));
			return "account";
		}
	}

	@GetMapping("/accountdetails")
	public String accountdetails(@RequestParam long id, HttpServletRequest req) {
		HttpSession session = req.getSession();
		AccountEnt accountEnt = accIFServiceLocal.findOne(id);	
		String[] parts = accountEnt.getAccountinfo().split(";");
		String[] image = accountEnt.getImage().split(";");
		req.setAttribute("accinfo", parts);
		req.setAttribute("image", image);
		req.setAttribute("acc", accountEnt);
		if (session.getAttribute("userid") == null || session.getAttribute("userid").equals("")) {
			return "accountdetails";
		} else {
			Long userid = (Long) session.getAttribute("userid");
			req.setAttribute("users", userService.findOne(userid));
			return "accountdetails";
		}
	}

	@GetMapping("/register")
	public String registerform(HttpServletRequest req) {
		return "register";
	}
	
	@GetMapping("/blog")
	public String blog(HttpServletRequest req) {
		HttpSession session = req.getSession();
		if (session.getAttribute("userid") == null || session.getAttribute("userid").equals("")) {
			return "blog";
		} else {
			Long userid = (Long) session.getAttribute("userid");
			req.setAttribute("users", userService.findOne(userid));
			return "blog";
		}
	}

	@GetMapping("/login")
	public String loginform(HttpServletRequest req) {
		return "login";
	}

	@PostMapping("/login")
	public void login(String username, String password, HttpServletRequest req, HttpServletResponse res) throws IOException {
		HttpSession session = req.getSession(true);
		try {
			UserEnt userEnt = userService.checkLogin(username, password);
			session.setAttribute("userid", userEnt.getId());
			Long userid = (Long) session.getAttribute("userid");
			req.setAttribute("users", userService.findOne(userid));
			if (userService.findOne(userid).getRole() == 1) {
				res.sendRedirect("/");
			}
			else {
				res.sendRedirect("admin");
			}
		} catch (Exception e) {
			res.sendRedirect("login");
		}
	}

	@GetMapping("/update")
	public String update(@RequestParam long id, HttpServletRequest req) {
		req.setAttribute("users", userService.findOne(id));
		return "index";
	}

	@PostMapping("/register")
	public String createUser(@ModelAttribute UserEnt userEnt,String confirm, HttpServletRequest req, HttpServletResponse res) throws IOException {
		HttpSession session = req.getSession(true);
		req.setAttribute("user", userEnt);
		req.setAttribute("confirm", confirm);
		if(userEnt.getEmail().isEmpty() || userEnt.getUsername().isEmpty() || userEnt.getPassword().isEmpty() || 
				userEnt.getFullname().isEmpty() || confirm.isEmpty()) {
			req.setAttribute("errornull", "* Thông tin không được để trống!");
			return "register";
		}
		else if(userEnt.getUsername().length() < 5 || userEnt.getPassword().length() < 5) {
			req.setAttribute("errorlength", "* Tài khoản và mật khẩu phải nhiều hơn 5 kí tự!");
			return "register";
		}
		else if(userEnt.getPassword().equals(confirm) == false) {
			req.setAttribute("errorconfirm", "* Xác nhận mật khẩu không đúng!");
			return "register";
		}
		else {
			if(userService.checkExitsUsername(userEnt.getUsername()) != null) {
				req.setAttribute("errorusername", "* Tài khoản này đã tồn tại!");
				return "register";
			}
			else if(userService.checkExitsEmail(userEnt.getEmail()) != null) {
				req.setAttribute("erroremail", "* Địa chỉ email này đã tồn tại!");
				return "register";
			}
			else {
				userEnt.setRole(1);
				userEnt.setStatus(1);
				userEnt.setBalance(BigInteger.ZERO);
				userService.save(userEnt);
				UserEnt userLog = userService.checkLogin(userEnt.getUsername(), userEnt.getPassword());
				session.setAttribute("userid", userLog.getId());
				Long userid = (Long) session.getAttribute("userid");
				req.setAttribute("users", userService.findOne(userid));
				res.sendRedirect("/");
				return "index";
			}
		}
	}
	
	/* ----------------------------CUSTOMER---------------------------- */
	
	@GetMapping("/usermanager")
	public String userManager(HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "USER-MANAGER");
		req.setAttribute("listuser", userService.findAllUser());
		return "admin";
	}
	
	@GetMapping("/useredit")
	public String getUser(@RequestParam long id, HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "USER-MANAGER");
		req.setAttribute("ACTION", "EDIT-USER");
		req.setAttribute("users", userService.findOne(id));
		return "admin";
	}
	
	@PostMapping("/checkout")
	public String checkout(@RequestParam long id, HttpServletRequest req) {
		HttpSession session = req.getSession();
		req.setAttribute("accinfo", accIFServiceLocal.findOne(id));
		if (session.getAttribute("userid") == null || session.getAttribute("userid").equals("")) {
			return "checkout";
		} else {
			Long userid = (Long) session.getAttribute("userid");
			req.setAttribute("users", userService.findOne(userid));
			return "checkout";
		}
	}
	
	@PostMapping("/payment")
	public String payment(@RequestParam long id, HttpServletRequest req) {
		HttpSession session = req.getSession();
		Long userid = (Long) session.getAttribute("userid");
		AccountEnt acc = accIFServiceLocal.findOne(id);
		UserEnt user = userService.findOne(userid);
		if (user.getBalance().compareTo( acc.getPrice()) > 0 || user.getBalance().compareTo( acc.getPrice()) > 1 ) {
			OrderEnt orderEnt = new OrderEnt();
			orderEnt.setStatus(1);
			orderEnt.setGame(acc.getGameEnt());
			orderEnt.setAccountEnts(acc);
			orderEnt.setTotal(acc.getPrice());
			orderEnt.setUser(user);
			orderEnt.setOrdercode("Ord#"+acc.getAccountid());
			user.setBalance(user.getBalance().subtract(acc.getPrice()));
			userService.save(user);
			orderServiceLocal.save(orderEnt);
			accIFServiceLocal.updateStatus(2, id);
			req.setAttribute("orderlist", orderServiceLocal.findByUserId(userid));
			req.setAttribute("users", userService.findOne(userid));
			return "orderhistory";
		}	
		else {
			req.setAttribute("accinfo", acc);
			req.setAttribute("users", userService.findOne(userid));
			req.setAttribute("error", "* Bạn không đủ tiền để thực hiện giao dịch này, vui lòng nạp thêm.");
			return "checkout";
		}
	}
	
	@GetMapping("/userorder")
	public String orderhistory(@RequestParam long id, HttpServletRequest req) {
		req.setAttribute("orderlist", orderServiceLocal.findByUserId(id));
		req.setAttribute("users", userService.findOne(id));
		return "orderhistory";
	}
	
	@GetMapping("/payin")
	public String payinhistory(@RequestParam long id, HttpServletRequest req) {
		req.setAttribute("MODE", "payh");
		req.setAttribute("paylist", paymentServiceLocal.findPaymentByID(id));
		req.setAttribute("users", userService.findOne(id));
		return "payin";
	}
	
	
	@PostMapping("/useredit")
	public String editUser(@ModelAttribute UserEnt userEnt, HttpServletRequest req) {
		UserEnt user = userService.findOne(userEnt.getId());
		user.setStatus(userEnt.getStatus());
		user.setRole(userEnt.getRole());
		userService.save(user);
		req.setAttribute("MODE", "USER-MANAGER");
		req.setAttribute("listuser", userService.findAllUser());
		return "admin";
	}
	
	/* ----------------------------GAME---------------------------- */
	
	@GetMapping("/gameedit")
	public String getGame(@RequestParam long id, HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "ADD-GAME");
		req.setAttribute("ACTION", "EDIT-GAME");
		req.setAttribute("game", gameService.findOne(id));
		return "admin";
	}
	
	@GetMapping("/gamedelete")
	public String deleteGame(@RequestParam long id, HttpServletRequest req) {
		gameService.delete(id);
		req.setAttribute("MODE", "ADD-GAME");
		req.setAttribute("listgame", gameService.findAllGame());
		return "admin";
	}
	
	@GetMapping("/viewaccount")
	public String viewAccount(@RequestParam long id, HttpServletRequest req) {
		req.setAttribute("MODE", "ADD-GAME");
		req.setAttribute("ACTION", "VIEW-ACCOUNT");
		GameEnt game = gameService.findOne(id);
		Collection<OrderEnt> orderbyDay = orderServiceLocal.findByCustomDay(1, id);
		Collection<OrderEnt> orderbyWeek = orderServiceLocal.findByCustomDay(7, id);
		Collection<OrderEnt> orderbyMonth = orderServiceLocal.findByCustomDay(30, id);
		BigInteger sumDay = BigInteger.ZERO;
		BigInteger sumWeek = BigInteger.ZERO;
		BigInteger sumMonth = BigInteger.ZERO;
		for (OrderEnt orderEnt : orderbyDay) {
			sumDay = sumDay.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyWeek) {
			sumWeek = sumWeek.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyMonth) {
			sumMonth = sumMonth.add(orderEnt.getTotal());
		}
		req.setAttribute("sumDay", sumDay);
		req.setAttribute("sumWeek", sumWeek);
		req.setAttribute("sumMonth", sumMonth);
		List<AccountEnt> accountlist = new ArrayList<AccountEnt>(game.getAccountEnts());
		List<AccountEnt> soldacc = new ArrayList<AccountEnt>();
		List<AccountEnt> activeacc = new ArrayList<AccountEnt>();
		for (AccountEnt accountEnt : accountlist) {
			if (accountEnt.getStatus() == 2) {
				soldacc.add(accountEnt);
			}
			else if (accountEnt.getStatus() == 1){
				activeacc.add(accountEnt);
			}
		}
		req.setAttribute("accountlist", accountlist);
		req.setAttribute("activeacc", activeacc);
		req.setAttribute("soldacc", soldacc);
		req.setAttribute("id", id);
		return "admin";
	}
	
	@PostMapping("/viewaccount")
	public String viewAccByDate(Long id, String date, String date1, HttpServletRequest req) throws ParseException {
		req.setAttribute("MODE", "ADD-GAME");
		req.setAttribute("ACTION", "VIEW-ACCOUNT");
		GameEnt game = gameService.findOne(id);
		Collection<OrderEnt> orderbyDate = orderServiceLocal.findByCustomDate(date, date1, id);
		Collection<OrderEnt> orderbyDay = orderServiceLocal.findByCustomDay(1, id);
		Collection<OrderEnt> orderbyWeek = orderServiceLocal.findByCustomDay(7, id);
		Collection<OrderEnt> orderbyMonth = orderServiceLocal.findByCustomDay(30, id);
		BigInteger sumDay = BigInteger.ZERO;
		BigInteger sumWeek = BigInteger.ZERO;
		BigInteger sumMonth = BigInteger.ZERO;
		BigInteger sumDate = BigInteger.ZERO;
		for (OrderEnt orderEnt : orderbyDay) {
			sumDay = sumDay.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyWeek) {
			sumWeek = sumWeek.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyMonth) {
			sumMonth = sumMonth.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyDate) {
			sumDate = sumDate.add(orderEnt.getTotal());
		}
		req.setAttribute("sumDay", sumDay);
		req.setAttribute("sumWeek", sumWeek);
		req.setAttribute("sumMonth", sumMonth);
		req.setAttribute("date", date);
		req.setAttribute("date1", date1);
		req.setAttribute("sumDate", sumDate);
		List<AccountEnt> accountlist = new ArrayList<AccountEnt>(game.getAccountEnts());
		List<AccountEnt> soldacc = new ArrayList<AccountEnt>();
		List<AccountEnt> activeacc = new ArrayList<AccountEnt>();
		for (AccountEnt accountEnt : accountlist) {
			if (accountEnt.getStatus() == 2) {
				soldacc.add(accountEnt);
			}
			else if (accountEnt.getStatus() == 1){
				activeacc.add(accountEnt);
			}
		}
		req.setAttribute("accountlist", accountlist);
		req.setAttribute("activeacc", activeacc);
		req.setAttribute("soldacc", soldacc);
		req.setAttribute("id", id);
		return "admin";
	}
	
	@PostMapping("/gameedit")
	public String editGame(@ModelAttribute GameEnt gameEnt, HttpServletRequest req) {
		gameService.save(gameEnt);
		req.setAttribute("MODE", "ADD-GAME");
		req.setAttribute("listgame", gameService.findAllGame());
		return "admin";
	}
	
	@GetMapping("/gamemanager")
	public String editGame(HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "ADD-GAME");
		req.setAttribute("listgame", gameService.findAllGame());
		return "admin";
	}
	
	@PostMapping("/gamemanager")
	public String createGame(@ModelAttribute GameEnt gameEnt, HttpServletRequest req) {
		gameEnt.setStatus(1);
		gameService.save(gameEnt);
		req.setAttribute("MODE", "ADD-GAME");
		req.setAttribute("listgame", gameService.findAllGame());
		return "admin";
	}
	
	
	/* ----------------------------GAME-ACCOUNT---------------------------- */
	
	@GetMapping("/accinfomanager")
	public String accform(HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "CR-ACCIF");
		req.setAttribute("game", gameService.findAllGame());
		req.setAttribute("listacc", accIFServiceLocal.findAllAcc());
		return "admin";
	}
	
	@GetMapping("/accinfoedit")
	public String getAccInfo(@RequestParam long id, HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "CR-ACCIF");
		req.setAttribute("ACTION", "EDIT-ACCIF");
		req.setAttribute("accif", accIFServiceLocal.findOne(id));
		req.setAttribute("gamelist", gameService.findAllGame());
		return "admin";
	}
	
	@GetMapping("/accinfodelete")
	public String deleteAccinfo(@RequestParam long id, HttpServletRequest req) {
		if (accIFServiceLocal.checkNullAcc(id) == null) {
			req.setAttribute("error", "Tài khoản này không tồn tại!");
			req.setAttribute("MODE", "CR-ACCIF");
			req.setAttribute("game", gameService.findAllGame());
			req.setAttribute("listacc", accIFServiceLocal.findAllAcc());
			return "admin";
		}
		else {
			accServiceLocal.deletebyIfID(id);
			accIFServiceLocal.delete(id);
			req.setAttribute("MODE", "CR-ACCIF");
			req.setAttribute("game", gameService.findAllGame());
			req.setAttribute("listacc", accIFServiceLocal.findAllAcc());
			return "admin";
		}
	}
	
	@PostMapping("/accinfoedit")
	public String editAccInfo(@ModelAttribute AccountEnt accountEnt,Account account, HttpServletRequest req) {
		String[] image = accountEnt.getImage().split(";");
		Long accid = Long.parseLong(req.getParameter("accid"));
		account.setId(accid);
		account.setAccountEnt(accountEnt);
		accountEnt.setCover(image[0]);
		if (accIFServiceLocal.checkNullAcc(accountEnt.getId()) == null) {
			req.setAttribute("MODE", "CR-ACCIF");
			req.setAttribute("ACTION", "EDIT-ACCIF");
			req.setAttribute("accif", accIFServiceLocal.findOne(accountEnt.getId()));
			req.setAttribute("gamelist", gameService.findAllGame());
			req.setAttribute("error", "Tài khoản không tồn tại!");
			return "admin";
		}
		else {
			accIFServiceLocal.save(accountEnt);
			accServiceLocal.save(account);
			req.setAttribute("MODE", "CR-ACCIF");
			req.setAttribute("game", gameService.findAllGame());
			req.setAttribute("listacc", accIFServiceLocal.findAllAcc());
			return "admin";
		}
	}
	
	@PostMapping("/accinfomanager")
	public String createAccount(@ModelAttribute AccountEnt accountEnt, Account account, HttpServletRequest req) {
		String[] image = accountEnt.getImage().split(";");
		if(accountEnt.getAccountid().isEmpty() || accountEnt.getAccountinfo().isEmpty() || accountEnt.getImage().isEmpty()
				|| accountEnt.getPrice() == null || account.getUsername().isEmpty() || account.getPassword().isEmpty()) {
			req.setAttribute("accif", accountEnt);
			req.setAttribute("acc", account);
			req.setAttribute("game", gameService.findAllGame());
			req.setAttribute("MODE", "CR-ACCIF");
			req.setAttribute("errornull", "* Các trường thông tin không được bỏ trống!");
			return "admin";
		}
		else {
			if(accIFServiceLocal.checkExitsAccID(accountEnt.getAccountid()) == null) {
				accountEnt.setCover(image[0]);
				accIFServiceLocal.save(accountEnt);
				account.setAccountEnt(accountEnt);
				accServiceLocal.save(account);
				req.setAttribute("MODE", "CR-ACCIF");
				req.setAttribute("listacc", accIFServiceLocal.findAllAcc());
				req.setAttribute("game", gameService.findAllGame());
				return "admin";
			}
			else {
				req.setAttribute("game", gameService.findAllGame());
				req.setAttribute("MODE", "CR-ACCIF");
				req.setAttribute("accif", accountEnt);
				req.setAttribute("acc", account);
				req.setAttribute("errorexits", "* Mã tài khoản này đã tồn tại!");
				return "admin";
			}
		}
	}
	
	
	/* ----------------------------ACCOUNT---------------------------- */
	
	@GetMapping("/accmanager")
	public String accforma(HttpServletRequest req) {
		req.setAttribute("MODE", "CR-ACC");
		req.setAttribute("accinfo", accIFServiceLocal.findAllAcc());
		return "admin";
	}
	
	@PostMapping("/accmanager")
	public String addaccount(@ModelAttribute Account account, HttpServletRequest req) {
		accServiceLocal.save(account);
		req.setAttribute("MODE", "CR-ACC");
		req.setAttribute("accinfo", accIFServiceLocal.findAllAcc());
		return "admin";
	}
	
	@GetMapping("/pay")
	public String payin(HttpServletRequest req) {
		HttpSession session = req.getSession();
		if (session.getAttribute("userid") == null || session.getAttribute("userid").equals("")) {
			return "payin";
		} else {
			Long userid = (Long) session.getAttribute("userid");
			req.setAttribute("users", userService.findOne(userid));
			return "payin";
		}
	}
	
	@PostMapping("/pay")
	public String pay(HttpServletRequest request, HttpServletResponse response,@RequestParam("price") double price ) throws MalformedURLException, IOException{
		String cancelUrl = Utils.getBaseURL(request) + "/" + URL_PAYPAL_CANCEL;
		String successUrl = Utils.getBaseURL(request) + "/" + URL_PAYPAL_SUCCESS;
		HttpSession session = request.getSession(true);
		session.setAttribute("paymentId", "");
		double vnd = price/20000;
		try {
			Payment payment = paypalService.createPayment(vnd, "USD", PaypalPaymentMethod.paypal, 
					PaypalPaymentIntent.sale,"payment description", cancelUrl, successUrl);
			for(Links links : payment.getLinks()){
				if(links.getRel().equals("approval_url")){
					return "redirect:" + links.getHref();				
				}
			}
		} catch (PayPalRESTException e) {
			log.error(e.getMessage());
		}
		return "redirect:/";
	}
	
	@GetMapping(URL_PAYPAL_CANCEL)
	public String cancelPay(){
		return "userinfo";
	}

	@GetMapping(URL_PAYPAL_SUCCESS)
	public String successPay(@RequestParam("paymentId") String paymentId,@RequestParam("token") String token, @RequestParam("PayerID") String payerId, HttpServletRequest req){
		HttpSession session = req.getSession(true);
		String pID = (String) session.getAttribute("paymentId");
		Long uID = (Long) session.getAttribute("userid");
		try {
			Payment payment = paypalService.executePayment(paymentId, payerId);
			Double vnd = Double.parseDouble(payment.getTransactions().get(0).getAmount().getTotal());
			if(payment.getState().equals("approved")){
				if(pID.equals(paymentId) == false) {
					UserEnt user = userService.findOne(uID);
					user.setBalance(user.getBalance().add(BigDecimal.valueOf(Math.round(vnd) * 20000).toBigInteger()));
					userService.save(user);
					PaymentEnt payh = new PaymentEnt(payment.getId(),payment.getPayer().getPayerInfo().getShippingAddress().getRecipientName(),
							payment.getPayer().getPayerInfo().getEmail(),vnd,BigDecimal.valueOf(Math.round(vnd) * 20000).toBigInteger(),1,user);
					paymentServiceLocal.save(payh);
					req.setAttribute("MODE", "payh");
					req.setAttribute("paylist", paymentServiceLocal.findPaymentByID(user.getId()));
					req.setAttribute("users", userService.findOne(user.getId()));
					session.setAttribute("paymentId", paymentId);
					return "payin";
				}
				else {
					req.setAttribute("users", userService.findOne(uID));
					System.out.println("khong duoc them tien dau hihihihihi");
					return "payin";
				}	
			}
		} catch (PayPalRESTException e) {
			log.error(e.getMessage());
		}
		return "redirect:/";
	}
	
	/* ----------------------------ORDER---------------------------- */
	
	
	@GetMapping("/ordermanager")
	public String getOrder(HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "VIEW-ORDER");
		req.setAttribute("orderlist", orderServiceLocal.findAllOrder());
		Collection<OrderEnt> orderbyDay = orderServiceLocal.findAllByCustomDay(1);
		Collection<OrderEnt> orderbyWeek = orderServiceLocal.findAllByCustomDay(7);
		Collection<OrderEnt> orderbyMonth = orderServiceLocal.findAllByCustomDay(30);
		BigInteger sumDay = BigInteger.ZERO;
		BigInteger sumWeek = BigInteger.ZERO;
		BigInteger sumMonth = BigInteger.ZERO;
		for (OrderEnt orderEnt : orderbyDay) {
			sumDay = sumDay.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyWeek) {
			sumWeek = sumWeek.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyMonth) {
			sumMonth = sumMonth.add(orderEnt.getTotal());
		}
		req.setAttribute("sumDay", sumDay);
		req.setAttribute("sumWeek", sumWeek);
		req.setAttribute("sumMonth", sumMonth);
		return "admin";
	}
	
	@PostMapping("/ordermanager")
	public String getOrderDate(HttpServletRequest req, String date, String date1) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "VIEW-ORDER");
		req.setAttribute("orderlist", orderServiceLocal.findAllByCustomDate(date, date1));
		Collection<OrderEnt> orderbyDate = orderServiceLocal.findAllByCustomDate(date, date1);
		Collection<OrderEnt> orderbyDay = orderServiceLocal.findAllByCustomDay(1);
		Collection<OrderEnt> orderbyWeek = orderServiceLocal.findAllByCustomDay(7);
		Collection<OrderEnt> orderbyMonth = orderServiceLocal.findAllByCustomDay(30);
		BigInteger sumDay = BigInteger.ZERO;
		BigInteger sumWeek = BigInteger.ZERO;
		BigInteger sumMonth = BigInteger.ZERO;
		BigInteger sumDate = BigInteger.ZERO;
		for (OrderEnt orderEnt : orderbyDay) {
			sumDay = sumDay.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyWeek) {
			sumWeek = sumWeek.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyMonth) {
			sumMonth = sumMonth.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyDate) {
			sumDate = sumDate.add(orderEnt.getTotal());
		}
		req.setAttribute("sumDate", sumDate);
		req.setAttribute("sumDay", sumDay);
		req.setAttribute("sumWeek", sumWeek);
		req.setAttribute("sumMonth", sumMonth);
		req.setAttribute("date", date);
		req.setAttribute("date1", date1);
		return "admin";
	}
	
	@GetMapping("/order")
	public String userOrder(@RequestParam("id") Long id, HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		Long userid = (Long) session.getAttribute("userid");
//		req.setAttribute("users", userService.findOne(userid));
		req.setAttribute("MODE", "VIEW-ORDER");
		req.setAttribute("hidden", "VIEW-ORDER");
		req.setAttribute("orderlist", orderServiceLocal.findByUserId(id));
		Collection<OrderEnt> orderbyDay = orderServiceLocal.findAllByUserDay(1, id);
		Collection<OrderEnt> orderbyWeek = orderServiceLocal.findAllByUserDay(7, id);
		Collection<OrderEnt> orderbyMonth = orderServiceLocal.findAllByUserDay(30, id);
		BigInteger sumDay = BigInteger.ZERO;
		BigInteger sumWeek = BigInteger.ZERO;
		BigInteger sumMonth = BigInteger.ZERO;
		for (OrderEnt orderEnt : orderbyDay) {
			sumDay = sumDay.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyWeek) {
			sumWeek = sumWeek.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyMonth) {
			sumMonth = sumMonth.add(orderEnt.getTotal());
		}
		req.setAttribute("sumDay", sumDay);
		req.setAttribute("sumWeek", sumWeek);
		req.setAttribute("sumMonth", sumMonth);		
		return "admin";
	}
	
	@GetMapping("/vieworder")
	public String getOrderbyDay(@RequestParam("id") Long id, @RequestParam("t") String type, HttpServletRequest req) {
		req.setAttribute("MODE", "VIEW-ORDER");
		req.setAttribute("hidden", "order-game");
		req.setAttribute("id", "id");
		BigInteger sum = BigInteger.ZERO;
		if(type.equals("day")) {
			req.setAttribute("orderlist", orderServiceLocal.findByCustomDay(1, id));
			for (OrderEnt orderEnt : orderServiceLocal.findByCustomDay(1, id)) {
				sum = sum.add(orderEnt.getTotal());
			}
			req.setAttribute("sum", sum);
		}
		else if(type.equals("week")) {
			req.setAttribute("orderlist", orderServiceLocal.findByCustomDay(7, id));
			for (OrderEnt orderEnt : orderServiceLocal.findByCustomDay(7, id)) {
				sum = sum.add(orderEnt.getTotal());
			}
			req.setAttribute("sum", sum);
		}
		else if(type.equals("month")) {
			req.setAttribute("orderlist", orderServiceLocal.findByCustomDay(30, id));
			for (OrderEnt orderEnt : orderServiceLocal.findByCustomDay(30, id)) {
				sum = sum.add(orderEnt.getTotal());
			}
			req.setAttribute("sum", sum);
		}
		return "admin";
	}
	
	@GetMapping("/vieworderacc")
	public String getOrderbyAcc(@RequestParam("id") Long id, HttpServletRequest req) {
		req.setAttribute("MODE", "VIEW-ORDER");
		req.setAttribute("hidden", "order-game");
		req.setAttribute("id", "id");
		BigInteger sum = BigInteger.ZERO;
		req.setAttribute("orderlist", orderServiceLocal.findByAccountId(id));
		for (OrderEnt orderEnt : orderServiceLocal.findByAccountId(id)) {
			sum = sum.add(orderEnt.getTotal());
		}
		req.setAttribute("sum", sum);
		return "admin";
	}
	
	@GetMapping("/vieworders")
	public String getAllOrderbyDay(@RequestParam("t") String type, HttpServletRequest req) {
		req.setAttribute("MODE", "VIEW-ORDER");
		Collection<OrderEnt> orderbyDay = orderServiceLocal.findAllByCustomDay(1);
		Collection<OrderEnt> orderbyWeek = orderServiceLocal.findAllByCustomDay(7);
		Collection<OrderEnt> orderbyMonth = orderServiceLocal.findAllByCustomDay(30);
		BigInteger sumDay = BigInteger.ZERO;
		BigInteger sumWeek = BigInteger.ZERO;
		BigInteger sumMonth = BigInteger.ZERO;
		for (OrderEnt orderEnt : orderbyDay) {
			sumDay = sumDay.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyWeek) {
			sumWeek = sumWeek.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyMonth) {
			sumMonth = sumMonth.add(orderEnt.getTotal());
		}
		req.setAttribute("sumDay", sumDay);
		req.setAttribute("sumWeek", sumWeek);
		req.setAttribute("sumMonth", sumMonth);		
		if(type.equals("day")) {
			req.setAttribute("orderlist", orderServiceLocal.findAllByCustomDay(1));
		}
		else if(type.equals("week")) {
			req.setAttribute("orderlist", orderServiceLocal.findAllByCustomDay(7));
		}
		else if(type.equals("month")) {
			req.setAttribute("orderlist", orderServiceLocal.findAllByCustomDay(30));
		}
		return "admin";
	}
	
	@PostMapping("/vieworders")
	public String getAllOrderbyDate(String date1, String date2, HttpServletRequest req) {
		req.setAttribute("MODE", "VIEW-ORDER");
		Collection<OrderEnt> orderbyDate = orderServiceLocal.findAllByCustomDate(date1, date2);
		Collection<OrderEnt> orderbyDay = orderServiceLocal.findAllByCustomDay(1);
		Collection<OrderEnt> orderbyWeek = orderServiceLocal.findAllByCustomDay(7);
		Collection<OrderEnt> orderbyMonth = orderServiceLocal.findAllByCustomDay(30);
		BigInteger sumDay = BigInteger.ZERO;
		BigInteger sumWeek = BigInteger.ZERO;
		BigInteger sumMonth = BigInteger.ZERO;
		BigInteger sumDate = BigInteger.ZERO;
		for (OrderEnt orderEnt : orderbyDay) {
			sumDay = sumDay.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyWeek) {
			sumWeek = sumWeek.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyMonth) {
			sumMonth = sumMonth.add(orderEnt.getTotal());
		}
		for (OrderEnt orderEnt : orderbyDate) {
			sumDate = sumDate.add(orderEnt.getTotal());
		}
		req.setAttribute("sumDate", sumDate);
		req.setAttribute("sumDay", sumDay);
		req.setAttribute("sumWeek", sumWeek);
		req.setAttribute("sumMonth", sumMonth);
		req.setAttribute("date", date1);
		req.setAttribute("date1", date2);
		req.setAttribute("orderlist", orderServiceLocal.findAllByCustomDate(date1, date2));
		return "admin";
	}
	
	@PostMapping("/vieworder")
	public String getOrderbyDate(Long id, String date1, String date2, HttpServletRequest req) {
		BigInteger sum = BigInteger.ZERO;
		req.setAttribute("hidden", "order-game");
		req.setAttribute("MODE", "VIEW-ORDER");
		req.setAttribute("id", "id");
		req.setAttribute("orderlist", orderServiceLocal.findByCustomDate(date1, date2, id));
		for (OrderEnt orderEnt : orderServiceLocal.findByCustomDate(date1, date2, id)) {
			sum = sum.add(orderEnt.getTotal());
		}
		req.setAttribute("sum", sum);
		return "admin";
	}
	
	
	//PAYIN
	
	@GetMapping("/payinmanager")
	public String getPayin(HttpServletRequest req) {
		req.setAttribute("MODE", "PAYIN");
		req.setAttribute("paylist", paymentServiceLocal.findAllPayment());
		BigInteger vnd = BigInteger.ZERO;
		for (PaymentEnt paymentEnt : paymentServiceLocal.findAllPayment()) {
			vnd = vnd.add(paymentEnt.getVnd());
		}
		req.setAttribute("vnd", vnd);
		return "admin";
	}

}

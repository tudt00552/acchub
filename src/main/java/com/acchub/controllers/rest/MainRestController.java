package com.acchub.controllers.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.acchub.entity.Account;
import com.acchub.entity.AccountEnt;
import com.acchub.entity.GameEnt;
import com.acchub.entity.OrderEnt;
import com.acchub.entity.UserEnt;
import com.acchub.service.AccIFServiceLocal;
import com.acchub.service.AccServiceLocal;
import com.acchub.service.GameServiceLocal;
import com.acchub.service.OrderServiceLocal;
import com.acchub.service.UserServiceLocal;


@RestController
public class MainRestController {
	
	@Autowired
	private UserServiceLocal IService;
	
	@Autowired
	private AccIFServiceLocal accIFServiceLocal;
	
	@Autowired
	private GameServiceLocal gameServiceLocal;
	
	@Autowired
	private AccServiceLocal accServiceLocal;
	
	@Autowired
	private OrderServiceLocal orderServiceLocal;
	
	@PostMapping(value = "/new")
	public UserEnt createUser(@RequestBody UserEnt model) {
		return IService.save(model);
	}
	
	@GetMapping("/findAllUser")
	public Collection<UserEnt> getAllUser() {
		return IService.findAllUser();
	}
	
	@GetMapping("/findAllOrder")
	public Collection<OrderEnt> getAllOrder() {
		return orderServiceLocal.findAllOrder();
	}
	
	@GetMapping("/findAllAcc")
	public Collection<AccountEnt> getAllAcc() {
		return accIFServiceLocal.findAllAcc();
	}
	
	@GetMapping("/findAllGame")
	public Collection<GameEnt> getAllGAme() {
		return gameServiceLocal.findAllGame();
	}
	
	@GetMapping("/findAllAccount")
	public Collection<Account> getAllAccount() {
		return accServiceLocal.findAllAccount();
	}
	
	@GetMapping("/findOneAcc")
	public AccountEnt getONeAcc() {
		return accIFServiceLocal.findOne(1);
	}
	
	@GetMapping("/findOneGame")
	public GameEnt getONeGame() {
		return gameServiceLocal.findOne(1);
	}
}

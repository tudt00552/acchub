<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<style type="text/css">
	<%@include file="../jsp/css/checkout.css"%>
</style>
<style type="text/css">
	<%@include file="../jsp/css/index.css"%>
</style>
<t:genericpage>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<div class="container">
	<div class="table-checkout">
		<h4><b>Xác nhận thanh toán cho tài khoản <span style="color: red">${accinfo.accountid}</span>.</b></h4>
		<table class="table" id="mytable">
			<tbody>
				<tr>
					<th>Tên game: </th>
					<td>${accinfo.gameEnt.title}</td>
					<td></td>
				</tr>
				<tr>
					<th>Chi tiết: </th>
					<td>${accinfo.accountinfo}</td>
					<td></td>
				</tr>
				<tr>
					<th>Giá tiền:</th>
					<td><strong style="color: red"><fmt:formatNumber type="number" pattern="###,###" value="${accinfo.price}"/> vnđ</strong></td>
				</tr>
				<tr>
					<td colspan="2">
						<form class="form-buy" method="post" action="payment">
							<input type="hidden" name="id" value="${accinfo.id} ">
							<button class="btn btn-success">Thanh Toán</button>
						</form>
						<span style="color: red">${error}</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
    </jsp:body>
</t:genericpage>
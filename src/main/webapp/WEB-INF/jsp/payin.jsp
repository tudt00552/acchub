<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<style type="text/css">
	<%@include file="../jsp/css/index.css"%>
</style>
<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
	<jsp:attribute name="footer">
    </jsp:attribute>
	<jsp:body>
		<div class="container">
		<div class="wrap">
				<c:choose>
			<c:when test="${MODE == 'payh'}">
				<div class="table">
					<h2>Payment History</h2>
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Created</th>
								<th>Customer</th>
								<th>PayID</th>
								<th>Payer Name</th>
								<th>Payer Email</th>
								<th>Total (USD)</th>
								<th>Receice (VNĐ)</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="payh" items="${paylist}">
								<tr>
									<td>${payh.id}</td>
									<td>${payh.createdAt}</td>
									<td>${payh.userp.fullname}</td>
									<td>${payh.payid}</td>
									<td>${payh.payername}</td>
									<td>${payh.payemail}</td>
									<td>${payh.usd} USD</td>
									<td><fmt:formatNumber type="number" pattern="###,###" value="${payh.vnd}"/> VNĐ</td>
									<td>${payh.status}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:when>
			<c:otherwise>
				<c:if test="${sessionScope.userid != null}">
					<div class="main-div">
						<div class="panel">
								<h1>Nạp Tiền</h1>
						</div>
						<form id="Login" action="pay" method="post">
							<div class="form-group">
								<input type="text" class="form-control" name="price"
							id="price" placeholder="Nhập số tiền bạn muốn nạp (VNĐ)">
							</div>
							<button type="submit" class="btn btn-primary">
								Nạp Tiền
							</button>
						</form>
						<div class="clear"></div>
					</div>
				</c:if>
				<c:if test="${sessionScope.userid == null}">
					<h1>Bạn cần đăng nhập để nạp tiền !</h1>
				</c:if>	
			</c:otherwise>
		</c:choose>
		</div>
		</div>
    </jsp:body>
</t:genericpage>
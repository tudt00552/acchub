<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">
	<%@include file="../jsp/css/index.css"%>
	<%@include file="../jsp/css/register.css"%>
</style>
<t:genericpage>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	    <div class="login-form">
        <div class="container">
            <div class="row main">
                <div class="main-center">
                    <form action="register" method="post">
                        <p>Đăng Ký Thành Viên</p>
                        <span style="color: red;">${errornull}</span>
                        <span style="color: red;">${errorlength}</span>
                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Tài Khoản</label>
                            <span style="color: red;">${errorusername}</span>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="username" value="${user.username}" id="name"  placeholder="Nhập Tài Khoản"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="cols-sm-2 control-label">Email</label>
                            <span style="color: red;">${erroremail}</span>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="email" value="${user.email}" id="email"  placeholder="Nhập Email"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="username" class="cols-sm-2 control-label">Họ Tên</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="fullname" value="${user.fullname}" id="username"  placeholder="Nhập Họ Tên"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="cols-sm-2 control-label">Mật Khẩu</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="password" value="${user.password}" id="password"  placeholder="Nhập Mật Khẩu"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="confirm" class="cols-sm-2 control-label">Xác Nhận Mật Khẩu</label>
                            <span style="color: red;">${errorconfirm}</span>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="confirm" value="${confirm}" id="confirm"  placeholder="Xác Nhận Mật Khẩu"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <button type="submit" class="btn btn-info btn-lg btn-block login-button">Đăng Kí</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </jsp:body>
</t:genericpage>
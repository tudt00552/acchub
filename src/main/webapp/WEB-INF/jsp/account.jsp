<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<style type="text/css">
	<%@include file="css/index.css"%>
	<%@include file="css/account.css"%>
</style>
<t:genericpage>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<div class="wrap">
    		<div class="footer-title">
			<div class="footer-title-content">
				<h1>${game.title}</h1>
				<h2>${game.description}</h2>
			</div>
			<div class="clear"></div>
		</div>
		<!-- End Footer-Title -->
		<!-- Start List -->
		<div class="about-us">
			<div class="about-us-content">
				<div class="about-us-list">
					<c:forEach items="${game.accountEnts}" var="acc">
							<c:if test="${acc.status == 1}">
								<div class="about-us-list-item">
								<img src="${acc.cover}">
								<p>Rank:kim cương</p>
								<p>Mã Số: ${acc.accountid}</p>
								<h1><fmt:formatNumber type="number" pattern="###,###" value="${acc.price}" /> vnđ</h1>
								<a href="accountdetails?id=${acc.id}" class="navbar-btn navbar-right btn_click">Xem chi tiết</a>
							</div>
							</c:if>
					</c:forEach>
					<div class="clear"></div>
				</div>
			</div>
		</div>
    	</div>
    </jsp:body>
</t:genericpage>
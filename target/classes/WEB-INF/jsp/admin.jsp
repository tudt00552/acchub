<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta charset="UTF-8">
<title>AccHub</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style type="text/css">
.form {
	margin-top: 20px !important;
}

.control-label {
	text-align: left !important;
}

.at {
	color: green;
	font-weight: bold;
}

.dat {
	color: red;
	font-weight: bold;
}

.abtn {
	background: #fff;
	border: none;
}

.udl {
	text-decoration: underline;
}
</style>
<script type="text/javascript">
	$(".modal").on("hidden.bs.modal", function() {
		$(".modal-body1").html("");
	});
</script>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Acchub</a>
				<p class="navbar-text">
					Signed in as
					<%-- ${users.fullname} --%>
					Đào Tuấn Tú<i class="glyphicon glyphicon-ok"></i>
				</p>
			</div>
			<div class="container">
				<p class="navbar-text navbar-right">
					<a href="logout" class="navbar-link">Logout <i
						class="glyphicon glyphicon-log-out"></i></a>
				</p>
			</div>
			<ul class="nav nav-tabs">
				<c:if test="${MODE == 'HOME'}">
					<li role="presentation" class="active"><a href="admin">Home</a></li>
				</c:if>
				<c:if test="${MODE != 'HOME'}">
					<li role="presentation"><a href="admin">Home</a></li>
				</c:if>
				<c:if test="${MODE == 'USER-MANAGER'}">
					<li role="presentation" class="active"><a href="usermanager">Customer</a></li>
				</c:if>
				<c:if test="${MODE != 'USER-MANAGER'}">
					<li role="presentation"><a href="usermanager">Customer</a></li>
				</c:if>
				<c:if test="${MODE == 'ADD-GAME'}">
					<li role="presentation" class="active"><a href="gamemanager">Game</a></li>
				</c:if>
				<c:if test="${MODE != 'ADD-GAME'}">
					<li role="presentation"><a href="gamemanager">Game</a></li>
				</c:if>
				<c:if test="${MODE == 'CR-ACC' || MODE == 'CR-ACCIF'}">
					<li role="presentation" class="active"><a
						href="accinfomanager">Account</a></li>
				</c:if>
				<c:if test="${MODE != 'CR-ACC' && MODE != 'CR-ACCIF'}">
					<li role="presentation"><a href="accinfomanager">Account</a></li>
				</c:if>
				<c:if test="${MODE == 'VIEW-ORDER'}">
					<li role="presentation" class="active"><a href="ordermanager">Order</a></li>
				</c:if>
				<c:if test="${MODE != 'VIEW-ORDER'}">
					<li role="presentation"><a href="ordermanager">Order</a></li>
				</c:if>
				<c:if test="${MODE == 'PAYIN'}">
					<li role="presentation" class="active"><a href="payinmanager">Payin</a></li>
				</c:if>
				<c:if test="${MODE != 'PAYIN'}">
					<li role="presentation"><a href="payinmanager">Payin</a></li>
				</c:if>
			</ul>
		</div>
	</nav>

	<div class="container form">
		<c:choose>

			<c:when test="${MODE == 'USER-MANAGER'}">
				<c:if test="${ACTION != 'EDIT-USER'}">
					<div class="table">
						<h2>Customer List</h2>
						<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Full Name</th>
									<th>Username</th>
									<th>Email</th>
									<th>Balance</th>
									<th>Role</th>
									<th>Order</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="user" items="${listuser}">
									<tr>
										<td>${user.id}</td>
										<td>${user.fullname}</td>
										<td>${user.username}</td>
										<td>${user.email}</td>
										<td><fmt:formatNumber type="number" pattern="###,###"
												value="${user.balance}" /> vnđ</td>
										<td><c:if test="${user.role == 1}">Customer</c:if> <c:if
												test="${user.role == 2}">
												<span class="at">Admin</span>
											</c:if></td>
										<td>${fn:length(user.orderlist)}</td>
										<td><c:if test="${user.status == 1}">Active</c:if> <c:if
												test="${user.status == 2}">Locked</c:if></td>
										<td><a class="btn" href="useredit?id=${user.id}"><i
												class="glyphicon glyphicon-pencil"></i></a><a class="btn"
											href="order?id=${user.id}"><i
												class="glyphicon glyphicon-search"></i></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
				<c:if test="${ACTION == 'EDIT-USER'}">
					<form class="form-horizontal" action="useredit" method="post">
						<input type="hidden" value="${users.id}" name="id">
						<div class="form-group">
							<label class="control-label col-sm-2" for="role">Role:</label>
							<div class="col-sm-10">
								<c:if test="${users.role == 1}">
									<select class="form-control" name="role" id="role">
										<option value="1" selected>Customer</option>
										<option value="2">Admin</option>
									</select>
								</c:if>
								<c:if test="${users.role == 2}">
									<select class="form-control" name="role" id="role">
										<option value="1">Customer</option>
										<option value="2" selected>Admin</option>
									</select>
								</c:if>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="status">Status:</label>
							<div class="col-sm-10">
								<c:if test="${users.status == 1}">
									<select class="form-control" name="status" id="status">
										<option value="1" selected>Active</option>
										<option value="2">Locked</option>
									</select>
								</c:if>
								<c:if test="${users.status == 2}">
									<select class="form-control" name="status" id="status">
										<option value="1">Active</option>
										<option value="2" selected>Locked</option>
									</select>
								</c:if>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">Submit</button>
							</div>
						</div>
					</form>
				</c:if>
			</c:when>
			
			<c:when test="${MODE == 'HOME'}">
				<div class="row">
					<div class="col-md-12">
						<h3>Total User: <span class="dat">${fn:length(user)}</span></h3>
						<h3>Total Game: <span class="dat">${fn:length(game)}</span></h3>
						<h3>Total Account: <span class="dat">${fn:length(gameacc)}</span></h3>
						<h4>+ Sold: <span class="dat">${fn:length(soldacc)}</span> - Earn: <span class="dat"><a style="color: red" href="ordermanager"><fmt:formatNumber type="number" pattern="###,###" value="${sum}" /> vnđ</a></span></h4>
						<h4>+ Active: <span class="at">${fn:length(activeacc)}</span></h4>
						<h3>Total Order: <span class="dat">${fn:length(order)}</span></h3>
						<h3>Total Payin: <span class="dat">${fn:length(payment)}</span></h3>
						<h4>+ Total Amount: <span class="dat"><fmt:formatNumber type="number" pattern="###,###" value="${sum1}" /> vnđ</span></h4>
					</div>
				</div>
			</c:when>

			<c:when test="${MODE == 'PAYIN'}">
				<div class="table">
					<div class="row">
						<div class="col-md-4">
							<h3>
								Total : <span style="color: red; font-weight: bold;"><fmt:formatNumber type="number" pattern="###,###" value="${vnd}"/> vnđ</span>
							</h3>
						</div>
					</div>
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Created</th>
								<th>Customer</th>
								<th>PayID</th>
								<th>Payer Name</th>
								<th>Payer Email</th>
								<th>Total (USD)</th>
								<th>Receice (VNĐ)</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="payh" items="${paylist}">
								<tr>
									<td>${payh.id}</td>
									<td>${payh.createdAt}</td>
									<td>${payh.userp.fullname}</td>
									<td>${payh.payid}</td>
									<td>${payh.payername}</td>
									<td>${payh.payemail}</td>
									<td>${payh.usd}USD</td>
									<td><fmt:formatNumber type="number" pattern="###,###"
											value="${payh.vnd}" /> VNĐ</td>
									<c:if test="${payh.status == 1}">
										<td><span class="at">Success</span></td>
									</c:if>
									<c:if test="${payh.status == 2}">
										<td><span class="at">Error</span></td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:when>

			<c:when test="${MODE == 'VIEW-ORDER'}">
				<div class="row">
					<div class="col-md-4">
						<h3>
							Total Order: <span style="color: blue; font-weight: bold;">${fn:length(orderlist)}</span>
						</h3>
					</div>
				</div>
				<hr>
				<div class="panel panel-default">
					<c:if test="${hidden != 'VIEW-ORDER' && hidden != 'order-game'}">
						<div class="panel-heading">
							<form class="form-inline" method="post" action="ordermanager">
								<label>Orders:&nbsp</label> <input type="hidden" value="${id}"
									name="id">
								<div class="form-group">
									<label for=""></label> <input type="date" class="form-control"
										value="${date}" id="date" name="date">
								</div>
								<div class="form-group">
									<label for="">&nbsp-&nbsp</label> <input type="date"
										class="form-control" value="${date1}" id="date1" name="date1">
								</div>
								<button type="submit" class="btn btn-default">Filter</button>
							</form>
						</div>
					</c:if>
					<div class="panel-body">
						<div class="row">
							<c:if test="${hidden != 'VIEW-ORDER' && hidden != 'order-game'}">
								<div class="col-md-4">
									<h4>
										Day:&nbsp<a href="vieworders?t=day"><span class="dat udl"><fmt:formatNumber
													type="number" pattern="###,###" value="${sumDay}" />vnđ</span></a>
									</h4>
								</div>
								<div class="col-md-4">
									<h4>
										Week:&nbsp<a href="vieworders?t=week"><span
											class="dat udl"><fmt:formatNumber type="number"
													pattern="###,###" value="${sumWeek}" /> vnđ</span></a>
									</h4>
								</div>
								<div class="col-md-4">
									<h4>
										Month:&nbsp<a href="vieworders?t=month"><span
											class="dat udl"><fmt:formatNumber type="number"
													pattern="###,###" value="${sumMonth}" /> vnđ</span></a>
									</h4>
								</div>
								<c:if test="${sumDate != null}">
									<div class="col-md-12">
										<h4>
											<form action="vieworders" method="post">
												<input type="hidden" name="id" value="${id}"> <input
													type="hidden" name="date1" value="${date}"> <input
													type="hidden" name="date2" value="${date1}">
												${date} to ${date1}:&nbsp
												<button class="abtn" type="submit">
													<span class="dat udl"><fmt:formatNumber
															type="number" pattern="###,###" value="${sumDate}" />
														vnđ</span>
												</button>
											</form>

										</h4>
									</div>
								</c:if>
							</c:if>
							<c:if test="${hidden == 'VIEW-ORDER' && hidden != 'order-game'}">
								<div class="col-md-4">
									<h4>
										Day:&nbsp<span class="dat"><fmt:formatNumber
												type="number" pattern="###,###" value="${sumDay}" />vnđ</span>
									</h4>
								</div>
								<div class="col-md-4">
									<h4>
										Week:&nbsp<span class="dat"><fmt:formatNumber
												type="number" pattern="###,###" value="${sumWeek}" /> vnđ</span>
									</h4>
								</div>
								<div class="col-md-4">
									<h4>
										Month:&nbsp<span class="dat"><fmt:formatNumber
												type="number" pattern="###,###" value="${sumMonth}" /> vnđ</span>
									</h4>
								</div>
							</c:if>
							<c:if test="${hidden == 'order-game'}">
								<div class="col-md-4">
									<h4>
										Total:&nbsp<span class="dat"><fmt:formatNumber
												type="number" pattern="###,###" value="${sum}" />vnđ</span>
									</h4>
								</div>
							</c:if>
						</div>
					</div>
				</div>
				<hr>
				<div class="table">
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Order Code</th>
								<th>Date Created</th>
								<th>Customer Name</th>
								<th>Customer Email</th>
								<th>Total</th>
								<th>Game Name</th>
								<th>Account ID</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="order" items="${orderlist}">
								<tr>
									<td>${order.id}</td>
									<td><a href="ordermanager">${order.ordercode}</a></td>
									<td>${order.createdAt}</td>
									<td>${order.user.fullname}</td>
									<td>${order.user.email}</td>
									<td><fmt:formatNumber type="number" pattern="###,###"
											value="${order.total}" /> vnđ</td>
									<td><a style="color: black"
										href="viewaccount?id=${order.accountEnts.gameEnt.id}">${order.accountEnts.gameEnt.title}</a></td>
									<td>${order.accountEnts.accountid}</td>
									<c:if test="${order.status == 1}">
										<td><span class="at">Complete</span></td>
									</c:if>
									<c:if test="${order.status == 2}">
										<td><span class="dat">error</span></td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:when>


			<c:when test="${MODE == 'CR-ACCIF'}">
				<c:choose>
					<c:when test="${ACTION == 'EDIT-ACCIF'}">
						<c:set var="formAction" value="accinfoedit" />
					</c:when>
					<c:otherwise>
						<c:set var="formAction" value="accinfomanager" />
					</c:otherwise>
				</c:choose>
				<form class="form-horizontal" action="${formAction}" method="post">
					<span style="color: red;">${error}</span>
					<input type="hidden" value="${accif.id}" name="id"> <span
						style="color: red">${errornull}</span>
					<div class="form-group">
						<label class="control-label col-sm-2" for="title">Accountid</label>
						<span style="color: red">${errorexits}</span>
						<div class="col-sm-10">
							<input type="text" value="${accif.accountid}"
								class="form-control" name="accountid" id="accountid"
								placeholder="Enter accountid">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="accountinfo">Account
							Info:</label>
						<div class="col-sm-10">
							<input type="text" value="${accif.accountinfo}"
								class="form-control" name="accountinfo" id="accountinfo"
								placeholder="Enter accountinfo">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="image">Image:</label>
						<div class="col-sm-10">
							<input type="text" value="${accif.image}" class="form-control"
								name="image" id="image" placeholder="Enter image">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="price">Price:</label>
						<div class="col-sm-10">
							<input type="number" value="${accif.price}" class="form-control"
								name="price" id="price" placeholder="Enter price">
						</div>
					</div>
					<c:choose>
						<c:when test="${ACTION == 'EDIT-ACCIF'}">
							<div class="form-group">
								<label class="control-label col-sm-2" for="gameEnt">Game:</label>
								<div class="col-sm-10">
									<select class="form-control" name="gameEnt" id="gameEnt">
										<option value="${accif.gameEnt.id}" selected>${accif.gameEnt.title}</option>
										<c:forEach var="game" items="${gamelist}">
											<c:if test="${game != accif.gameEnt}">
												<option value="${game.id}">${game.title}</option>
											</c:if>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="form-group">
								<label class="control-label col-sm-2" for="gameEnt">Game:</label>
								<div class="col-sm-10">
									<select class="form-control" name="gameEnt" id="gameEnt">
										<c:forEach var="game" items="${game}">
											<option value="${game.id}">${game.title}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${ACTION == 'EDIT-ACCIF'}">
							<input type="hidden" value="${accif.account.id}" name="accid">
							<div class="form-group">
								<label class="control-label col-sm-2" for="username">Username:</label>
								<div class="col-sm-10">
									<input type="text" value="${accif.account.username}"
										class="form-control" name="username" id="username"
										placeholder="Enter username">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="password">Password:</label>
								<div class="col-sm-10">
									<input type="text" value="${accif.account.password}"
										class="form-control" name="password" id="password"
										placeholder="Enter password">
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="form-group">
								<label class="control-label col-sm-2" for="username">Username:</label>
								<div class="col-sm-10">
									<input type="text" value="${acc.username}" class="form-control"
										name="username" id="username" placeholder="Enter username">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="password">Password:</label>
								<div class="col-sm-10">
									<input type="text" value="${acc.password}" class="form-control"
										name="password" id="password" placeholder="Enter password">
								</div>
							</div>
						</c:otherwise>
					</c:choose>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-default">Submit</button>
						</div>
					</div>
				</form>
				<hr>
				<c:if test="${ACTION != 'EDIT-ACCIF'}">
					<div class="table">
						<h2>Account List</h2>
						<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Image</th>
									<th>Acc ID</th>
									<th>Account Info</th>
									<th>Price</th>
									<th>Game</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="acc" items="${listacc}">
									<tr>
										<td>${acc.id}</td>
										<td><img src="${acc.cover}" width="50px;" /></td>
										<td>${acc.accountid}</td>
										<td>${acc.accountinfo}</td>
										<td><fmt:formatNumber type="number" pattern="###,###"
												value="${acc.price}" /> vnđ</td>
										<td><a href="viewaccount?id=${acc.gameEnt.id}"
											style="color: black">${acc.gameEnt.title}</a></td>
										<c:if test="${acc.status == 1}">
											<td class="at">Active</td>
										</c:if>
										<c:if test="${acc.status == 2}">
											<td class="dat">Sold</td>
										</c:if>
										<td><a class="btn" href="accinfoedit?id=${acc.id}"><i
												class="glyphicon glyphicon-pencil"></i></a> <a class="btn"
											href="accinfodelete?id=${acc.id}"><i
												class="glyphicon glyphicon-trash"></i></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
			</c:when>

			<c:when test="${MODE == 'ADD-GAME'}">
				<c:if test="${ACTION != 'VIEW-ACCOUNT'}">
					<c:choose>
						<c:when test="${ACTION == 'EDIT-GAME'}">
							<c:set var="formAction" value="gameedit" />
						</c:when>
						<c:otherwise>
							<c:set var="formAction" value="gamemanager" />
						</c:otherwise>
					</c:choose>
					<form class="form-horizontal" action="${formAction}" method="post">
						<input type="hidden" name="id" value="${game.id}">
						<div class="form-group">
							<label class="control-label col-sm-2" for="title">Title</label>
							<div class="col-sm-10">
								<input type="text" value="${game.title}" class="form-control"
									name="title" id="title" placeholder="Enter title">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="image">Image:</label>
							<div class="col-sm-10">
								<input type="text" value="${game.image}" class="form-control"
									name="image" id="image" placeholder="Enter image">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="description">Description:</label>
							<div class="col-sm-10">
								<input type="text" value="${game.description}"
									class="form-control" name="description" id="description"
									placeholder="Enter description">
							</div>
						</div>
						<c:if test="${ACTION == 'EDIT-GAME'}">
							<div class="form-group">
								<label class="control-label col-sm-2" for="description">Status:</label>
								<div class="col-sm-10">
									<input type="text" value="${game.status}"
										class="form-control" name="status" id="status"
										placeholder="Enter status">
								</div>
							</div>
						</c:if>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">Submit</button>
							</div>
						</div>
					</form>
					<c:if test="${ACTION == 'EDIT-GAME'}">
						<b>List Account :</b>
						<c:forEach var="acc" items="${game.accountEnts}">
							<p>
								<img src="${acc.cover}" width="50px"> - ${acc.accountid} -
								${acc.price} vnđ <a class="btn" href="accinfoedit?id=${acc.id}"><i
									class="glyphicon glyphicon-pencil"></i></a>
							</p>
						</c:forEach>
					</c:if>
					<hr>
				</c:if>
				<c:if test="${ACTION != 'EDIT-GAME' && ACTION != 'VIEW-ACCOUNT'}">
					<div class="table">
						<h2>Game List</h2>
						<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Image</th>
									<th>Created</th>
									<th>Title</th>
									<th>Description</th>
									<th>Total Account</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="game" items="${listgame}">
									<tr>
										<td>${game.id}</td>
										<td><img src="${game.image}" width="50px;" /></td>
										<td>${game.createdAt}</td>
										<td>${game.title}</td>
										<td>${game.description}</td>
										<td>${fn:length(game.accountEnts)}</td>
										<c:if test="${game.status == 1}">
											<td class="at">Active</td>
										</c:if>
										<c:if test="${game.status == 2}">
											<td class="dat">Deactive</td>
										</c:if>
										<td><a class="btn" href="viewaccount?id=${game.id}"><i
												class="glyphicon glyphicon-search"></i></a><a class="btn"
											href="gameedit?id=${game.id}"><i
												class="glyphicon glyphicon-pencil"></i></a> <a class="btn"
											href="gamedelete?id=${game.id}"><i
												class="glyphicon glyphicon-trash"></i></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
				<c:if test="${ACTION == 'VIEW-ACCOUNT'}">
					<h2>${orderlist}</h2>
					<div class="row">
						<div class="col-md-4">
							<h3>
								Total Account: <span style="color: blue; font-weight: bold;">${fn:length(accountlist)}</span>
							</h3>
						</div>
						<div class="col-md-4">
							<h3>
								Active: <span style="color: green; font-weight: bold;">${fn:length(activeacc)}</span>
							</h3>
						</div>
						<div class="col-md-4">
							<h3>
								Sold: <span style="color: red; font-weight: bold;">${fn:length(soldacc)}</span>
							</h3>
						</div>
					</div>
					<hr>
					<div class="panel panel-default">
						<div class="panel-heading">
							<form class="form-inline" method="post" action="viewaccount">
								<label>Earnings:&nbsp</label> <input type="hidden" value="${id}"
									name="id">
								<div class="form-group">
									<label for=""></label> <input type="date" class="form-control"
										value="${date}" id="date" name="date">
								</div>
								<div class="form-group">
									<label for="">&nbsp-&nbsp</label> <input type="date"
										class="form-control" value="${date1}" id="date1" name="date1">
								</div>
								<button type="submit" class="btn btn-default">Filter</button>
							</form>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<h4>
										Day:&nbsp<a href="vieworder?id=${id}&t=day"><span
											class="dat udl"><fmt:formatNumber type="number"
													pattern="###,###" value="${sumDay}" />vnđ</span></a>
									</h4>
								</div>
								<div class="col-md-4">
									<h4>
										Week:&nbsp<a href="vieworder?id=${id}&t=week"><span
											class="dat udl"><fmt:formatNumber type="number"
													pattern="###,###" value="${sumWeek}" /> vnđ</span></a>
									</h4>
								</div>
								<div class="col-md-4">
									<h4>
										Month:&nbsp<a href="vieworder?id=${id}&t=month"><span
											class="dat udl"><fmt:formatNumber type="number"
													pattern="###,###" value="${sumMonth}" /> vnđ</span></a>
									</h4>
								</div>
								<c:if test="${sumDate != null}">
									<div class="col-md-12">
										<h4>
											<form action="vieworder" method="post">
												<input type="hidden" name="id" value="${id}"> <input
													type="hidden" name="date1" value="${date}"> <input
													type="hidden" name="date2" value="${date1}">
												${date} to ${date1}:&nbsp
												<button class="abtn" type="submit">
													<span class="dat udl"><fmt:formatNumber
															type="number" pattern="###,###" value="${sumDate}" />
														vnđ</span>
												</button>
											</form>

										</h4>
									</div>
								</c:if>
							</div>
						</div>
					</div>
					<hr>
					<div class="table">
						<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Image</th>
									<th>Acc ID</th>
									<th>Price</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="acc" items="${accountlist}">
									<tr>
										<td>${acc.id}</td>
										<td><img src="${acc.cover}" width="50px;" /></td>
										<td>${acc.accountid}</td>
										<td><fmt:formatNumber type="number" pattern="###,###"
												value="${acc.price}" /> vnđ</td>
										<c:if test="${acc.status == 1}">
											<td class="at">Active</td>
											<td><a class="btn at" href="accinfoedit?id=${acc.id}"><i
													class="glyphicon glyphicon-pencil"></i></a></td>
										</c:if>
										<c:if test="${acc.status == 2}">
											<td class="dat">Sold</td>
											<td><a class="btn dat" href="vieworderacc?id=${acc.id}"><i
													class="glyphicon glyphicon-search"></i></a></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
			</c:when>

			<c:when test="${MODE == 'CR-ACC'}">
				<form class="form-horizontal" action="accmanager" method="post">
					<div class="form-group">
						<label class="control-label col-sm-2" for="username">Username:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="username"
								id="username" placeholder="Enter username">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="password">Password:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="password"
								id="password" placeholder="Enter password">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="accountEnt">Account
							ID:</label>
						<div class="col-sm-10">
							<select class="form-control" name="accountEnt" id="accountEnt">
								<c:forEach var="acc" items="${accinfo}">
									<option value="${acc.id}">${acc.id}-${acc.accountid}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-default">Submit</button>
						</div>
					</div>
				</form>
			</c:when>
		</c:choose>



		<!-- --ACCOUNT FORM-- -->


	</div>



</body>
</html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<style type="text/css">
	<%@include file="../jsp/css/index.css"%>
</style>
<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
	<jsp:attribute name="footer">
    </jsp:attribute>
	<jsp:body>
    	<div class="container">
    		<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Order Code</th>
								<th>Date Created</th>
								<th>Total</th>
								<th>Game Name</th>
								<th>Account ID</th>
								<th>Account Info</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="order" items="${orderlist}">
								<tr>
									<td>${order.id}</td>
									<td><a href="ordermanager">${order.ordercode}</a></td>
									<td>${order.createdAt}</td>
									<td><fmt:formatNumber type="number" pattern="###,###" value="${order.total}"/> vnđ</td>
									<td>${order.accountEnts.gameEnt.title}</td>
									<td>${order.accountEnts.accountid}</td>
									<td><a data-toggle="modal" data-target="#myModal${order.id}"><i
									class="glyphicon glyphicon-eye-open"></i></a>
										<div class="modal fade" id="myModal${order.id}" role="dialog">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Account Info</h4>
													</div>
													<div class="modal-body">
														<table>
															<tr>
																<th>Username:&nbsp</th>
																<td>${order.accountEnts.account.username}</td>
															</tr>
															<tr>
																<th>Password:&nbsp</th>
																<td>${order.accountEnts.account.password}</td>
															</tr>
														</table>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default"
													data-dismiss="modal">Close</button>
													</div>
												</div>

											</div>
										</div></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
    	</div>
    </jsp:body>
</t:genericpage>
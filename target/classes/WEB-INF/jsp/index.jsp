<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style type="text/css">
	<%@include file="../jsp/css/index.css"%>
</style>
<t:genericpage>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<body class="ibody">
	<div class="wrap">
		<!-- End slide -->
		<!-- Start List -->
		<div class="container">
			<div class="about-us">
				<div class="about-us-content">
					<div class="about-us-caption">
						<h1>Danh Mục Game</h1>
					</div>
					<div class="row">
					<c:forEach var="game" items="${gameacc}">
					<c:if test="${game.id == 1 && game.status == 1}">
						<div class="about-us-list-item col-md-3 col-lg-6">
								<img src="https://nick.vn/storage/images/TBCOY2evmo_1560578845.jpg">
								<a href="account?id=${game.id}"><span>${game.title}</span></a>
								<p>Số tài khoản: ${fn:length(game.accountEnts)}</p>
								<a href="account?id=${game.id}" class="navbar-btn navbar-right btn_click">Xem thêm</a>
						</div>
					</c:if>
					<c:if test="${game.id != 1 && game.status == 1}">
						<div class="about-us-list-item col-md-3 col-lg-3">
							<img src="${game.image}">
							<a href="account?id=${game.id}"><span>${game.title}</span></a>
							<p>Số tài khoản: ${fn:length(game.accountEnts)}</p>
							<a href="account?id=${game.id}" class="navbar-btn navbar-right btn_click">Xem thêm</a>
						</div>
					</c:if>
					</c:forEach>
					</div>
			</div>
		</div>

		<div class="about-us">
			<div class="about-us-content">
				<div class="about-us-caption">
					<h1>Danh Mục Random</h1>
				</div>
				<div class="row">
				
					<div class="about-us-list-item col-md-3 col-lg-3">
							<img src="https://nick.vn/storage/images/sRdfJdqIQX_1560539371.jpg">
							<a href=""><span>Mua Acc Liên Quân Giá: 12k</span></a>
							<p>Số tài khoản: 132,009</p>
							<p>Đã bán: 250,505</p>
							<a href="" class="navbar-btn navbar-right btn_click">Xem thêm</a>
					</div>
					<div class="about-us-list-item col-md-3 col-lg-3">
						<img src="https://nick.vn/storage/images/K4m9S4Tcbr_1560539399.jpg">
						<a href=""><span>Mua Acc Liên Quân Giá: 32k</span></a>
						<p>Số tài khoản: 79,009</p>
						<p>Đã bán: 150,505</p>
						<a href="" class="navbar-btn navbar-right btn_click">Xem thêm</a>
					</div>
					<div class="about-us-list-item col-md-3 col-lg-3">
						<img src="https://nick.vn/storage/images/Z3xAR2YeZy_1560584622.jpg">
						<a href=""><span>Mua Acc Liên Quân Giá: 32k</span></a>
						<p>Số tài khoản: 57,009</p>
						<p>Đã bán: 100,505</p>
						<a href="" class="navbar-btn navbar-right btn_click">Xem thêm</a>
					</div>
					<div class="about-us-list-item col-md-3 col-lg-3">
						<img src="https://nick.vn/storage/images/J8D49zyKNQ_1560585631.jpg">
						<a href=""><span>Mua Acc Liên Quân Giá: 60k</span></a>
						<p>Số tài khoản: 44,009</p>
						<p>Đã bán: 70,505</p>
						<a href="" class="navbar-btn navbar-right btn_click">Xem thêm</a>
					</div>
					<div class="about-us-list-item col-md-3 col-lg-3">
						<img src="https://nick.vn/storage/images/IpzNOw5c46_1568252297.jpg">
						<a href=""><span>Mua Acc FreeFire Giá: 132k</span></a>
						<p>Số tài khoản: 54,009</p>
						<p>Đã bán: 85,505</p>
						<a href="" class="navbar-btn navbar-right btn_click">Xem thêm</a>
					</div>
					<div class="about-us-list-item col-md-3 col-lg-3">
						<img src="https://nick.vn/storage/images/hQEH8ZKXMj_1560504935.jpg">
						<a href=""><span>Mua Acc FreeFire Giá: 160k</span></a>
						<p>Số tài khoản: 65,009</p>
						<p>Đã bán: 80,505</p>
						<a href="" class="navbar-btn navbar-right btn_click">Xem thêm</a>
					</div>
					<div class="about-us-list-item col-md-3 col-lg-3">
						<img src="https://nick.vn/storage/images/9qf7Oqa090_1562920193.jpg">
						<a href=""><span>Mua Acc Liên Minh Giá: 130k</span></a>
						<p>Số tài khoản: 245,309</p>
						<p>Đã bán: 470,505</p>
						<a href="" class="navbar-btn navbar-right btn_click">Xem thêm</a>
					</div>
					<div class="about-us-list-item col-md-3 col-lg-3">
						<img src="https://nick.vn/storage/images/QGSpkSa6Md_1560539907.jpg">
						<a href=""><span>Mua Acc PubgMobile Giá:40k</span></a>
						<p>Số tài khoản: 144,609</p>
						<p>Đã bán: 270,505</p>
						<a href="" class="navbar-btn navbar-right btn_click">Xem thêm</a>
					</div>
					
					<div class="clear"></div>
				
				
			    	</div>
				</div>
			</div>
		</div>
	</div>
</body>
    </jsp:body>
</t:genericpage>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">
	<%@include file="../jsp/css/index.css"%>
	<%@include file="../jsp/css/login.css"%>
</style>
<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
	<jsp:attribute name="footer">
    </jsp:attribute>
	<jsp:body>
    	<div id="LoginForm">
    		<div class="container">
				<div class="login-form">
					<div class="main-div">
						<div class="panel">
							<h2>Đăng nhập</h2>
						</div>
						<form id="Login" action="login" method="post">

							<div class="form-group">
								<input type="username" class="form-control" name="username"
								id="username" placeholder="Tài khoản"> <i
								class="fas fa-user"></i>
							</div>
							<div class="form-group">
								<input type="password" class="form-control" name="password"
								id="password" placeholder="Mật khẩu"> <i
								class="fas fa-lock"></i>
							</div>
							<button type="submit" class="btn btn-primary">
								Đăng Nhập
							</button>
							<a href="register"
							class="btn  btn-social btn-google btn-flat d-inline-block">
								<i class="fas fa-key"></i>Tạo tài khoản
							</a>

						</form>
						<div class="clear"></div>
					</div>
				</div>
			</div>
    	</div>
    </jsp:body>
</t:genericpage>
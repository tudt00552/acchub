<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<style type="text/css">
	<%@include file="css/index.css" %>
  	<%@include file="css/detail.css" %>
</style>
<t:genericpage>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<div class="wrap">
    		<div class="footer-title">
			<div class="footer-title-content">
				<h1>Thông Tin Chi Tiết</h1>
				<h2>Tên Game: <span> ${acc.gameEnt.title} </span> </h2>
				<p>Mã Số: <span style="color: red; font-weight: bold;">${acc.accountid}</span></p>
				<p style="font-weight: bold;">Thông tin: </p>
				<c:forEach var="info" items="${accinfo}">
					<p>+ ${info}</p>
				</c:forEach>
				<p>Giá: <span style="color: red"><fmt:formatNumber type="number" pattern="###,###" value="${acc.price}"/> vnđ</span></p>
			</div>
			<c:if test="${sessionScope.userid == null}">
					<form class="form-buy" action="login" method="get">
						<button type="submit" class="btn btn-success btn-lg btn-block">Mua Ngay</button>
					</form>
					<span style="color: red">* bạn cần <a href="login">ĐĂNG NHẬP</a> để mua tài khoản này!</span>
				</c:if>
				<c:if test="${sessionScope.userid != null}">
					<form class="form-buy" action="checkout" method="post">
					<input type="hidden" value="${acc.id}" name="id">
					<button type="submit" class="btn btn-success btn-lg btn-block">Mua Ngay</button>
				</form>
				</c:if>
			<div class="clear"></div>
			</div>
			<!-- End Footer-Title -->
			
			<!-- Start List-image -->
			<div class="List-image container">
			<div class="row">
				<c:forEach items="${image}" var="img">
					<div class="col-md-6">
						<img src="${img}">
					</div>
				</c:forEach>
			</div>	
			</div>
			<div class="clear"></div>
    	</div>
    </jsp:body>
</t:genericpage>
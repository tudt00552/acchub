<!DOCTYPE html>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true"%>
<%@attribute name="footer" fragment="true"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="UTF-8">
<title>AccHub</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head><style type="text/css">
	<%@include file="../jsp/css/index1.css"%>
</style>
<body>
	<div id="pageheader">
		<jsp:invoke fragment="header" />
		<div class="header">
			<div class="header-content">
				<i class="fa fa-phone"></i> <span>0862862338</span> <i
					class="fa fa-envelope-square"></i> <span>acchub@fpt.edu.vn</span>
				<a href="https://www.facebook.com/VTT.info"><img
					src="http://icons.iconarchive.com/icons/limav/flat-gradient-social/24/Instagram-icon.png"></a>
				<a href=""><img src=""></a>
			</div>
		</div>
		<!-- End Header -->
		<!-- Start navigation -->
		<div id="myHeader" class="myNavbar sticky-header">
			<div class="container">
				<div class="myNavbar-content ">
					<div class="myNavbar-brand col-xs-6 col-sm-2 col-md-1">
						<img
							src="https://www.freelogodesign.org/file/app/client/thumb/d08bb035-cb28-4943-abd8-a3f918de0537_200x200.png?1572879690963">
					</div>
					<div class="myNavbar-menu col-sm-8 col-md-8">
						<ul>
							<li class="active"><a href="/">TRANG CHỦ</a></li>
							<li><a href="pay">NẠP TIỀN</a></li>
							<li><a href="#">NICK RANDOM</a></li>
							<li><a href="pay">DỊCH VỤ</a></li>
							<li><a href="blog">TIN TỨC</a></li>
						</ul>
					</div>
					<c:if test="${sessionScope.userid != null}">
						<div class="social col-xs-5 col-sm-2 col-md-3">
							<div class="social-item dropdown">
								<a href="/"
									class="navbar-btn navbar-right register btn_click btn_click_log dropdown-toggle"
									id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="true"> <span class="usernamelog">${users.fullname}
										| <fmt:formatNumber type="number" pattern="###,###" value="${users.balance}"/> vnđ<span class="caret"></span>
								</span>
								</a>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="logout">Logout</a></li>
									<li><a href="userorder?id=${users.id}">Order History</a></li>
									<li><a href="payin?id=${users.id}">Payin History</a></li>
								</ul>
							</div>
						</div>
					</c:if>
					<c:if test="${sessionScope.userid == null}">
						<div class="social col-xs-5 col-sm-2 col-md-3">
							<div class="social-item">
								<a href="login" class="navbar-btn login navbar-right btn_click">Đăng
									Nhập</a>
							</div>
							<div class="social-item">
								<a href="register"
									class="navbar-btn navbar-right register btn_click">Đăng Ký</a>
							</div>
						</div>
					</c:if>
					<!-- Clear -->
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="body">
		<jsp:doBody />
	</div>
	<div id="pagefooter">
		<jsp:invoke fragment="footer" />
		<section id="carousel">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="quote">
							<i class="fa fa-quote-left fa-4x"></i>
						</div>
						<div class="carousel slide" id="fade-quote-carousel"
							data-ride="carousel" data-interval="3000">
							<!-- Carousel indicators -->
							<ol class="carousel-indicators">
								<li data-target="#fade-quote-carousel" data-slide-to="0"></li>
								<li data-target="#fade-quote-carousel" data-slide-to="1"></li>
								<li data-target="#fade-quote-carousel" data-slide-to="2"
									class="active"></li>
								<li data-target="#fade-quote-carousel" data-slide-to="3"></li>
								<li data-target="#fade-quote-carousel" data-slide-to="4"></li>
							</ol>
							<!-- Carousel items -->
							<div class="carousel-inner aubout">
								<div class="item">
									<div class="profile-circle"
										style="background-image: url(https://cdngarenanow-a.akamaihd.net/mgames/kgcenter/tw/Art_Resources/UI/Dynamic/Skill/12900.png);"></div>
									<blockquote>
										<h3>Vũ Trí Thành</h3>
										<h4>Osin</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Quidem, veritatis nulla eum laudantium totam tempore
											optio doloremque laboriosam quas, quos eaque molestias odio
											aut eius animi. Impedit temporibus nisi accusamus.</p>
									</blockquote>
								</div>
								<div class="item">
									<div class="profile-circle"
										style="background-image: url(https://vignette.wikia.nocookie.net/arenaofvalor/images/f/fd/Ngu%E1%BB%93n_C%C6%A1n_R%E1%BA%AFc_R%E1%BB%91i.png/revision/latest?cb=20190703103654&path-prefix=vi);"></div>
									<blockquote>
										<h3>Đào Tuấn Tú</h3>
										<h4>Chủ tịch</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Quidem, veritatis nulla eum laudantium totam tempore
											optio doloremque laboriosam quas, quos eaque molestias odio
											aut eius animi. Impedit temporibus nisi accusamus.</p>
									</blockquote>
								</div>
								<div class="active item">
									<div class="profile-circle"
										style="background-image: url(http://netblogpro.com/wp-content/uploads/2017/02/long-kich.png);"></div>
									<blockquote>
										<h3>Vũ Trí Thành</h3>
										<h4>Osin</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Quidem, veritatis nulla eum laudantium totam tempore
											optio doloremque laboriosam quas, quos eaque molestias odio
											aut eius animi. Impedit temporibus nisi accusamus.</p>
									</blockquote>
								</div>
								<div class="item">
									<div class="profile-circle"
										style="background-image: url(http://netblogpro.com/wp-content/uploads/2017/02/long-kich.png);"></div>
									<blockquote>
										<h3>Đào Tuấn Tú</h3>
										<h4>Chủ tịch</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Quidem, veritatis nulla eum laudantium totam tempore
											optio doloremque laboriosam quas, quos eaque molestias odio
											aut eius animi. Impedit temporibus nisi accusamus.</p>
									</blockquote>
								</div>
								<div class="item">
									<div class="profile-circle"
										style="background-image: url(http://netblogpro.com/wp-content/uploads/2017/02/skill-cong-kich-kich-dong-cua-tuong-mganga.png);"></div>
									<blockquote>
										<h3>Vũ Trí Thành</h3>
										<h4>Osin</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Quidem, veritatis nulla eum laudantium totam tempore
											optio doloremque laboriosam quas, quos eaque molestias odio
											aut eius animi. Impedit temporibus nisi accusamus.</p>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--Start footer-->
		<footer id="colophon" class="site-footer ">
			<div class="container">
				<div class="footer-widgets-area">
					<div
						class="sidebar-footer footer-columns footer-4-columns clearfix">
						<div id="footer-1" class="footer-1 footer-column widget-area"
							role="complementary">
							<aside id="text-27" class="widget widget_text">
								<h3 class="widget-title">THÔNG TIN</h3>
								<div class="textwidget"></div>
							</aside>
							<aside id="wpnetbase-image-banner-14"
								class="widget widget_wpnetbase-image-banner">
								<div
									class="so-widget-wpnetbase-image-banner so-widget-wpnetbase-image-banner-default-3f547a15eaaa">
									<div class="nbt-image-banner-widget ">
										<img src="https://www.freelogodesign.org/file/app/client/thumb/d08bb035-cb28-4943-abd8-a3f918de0537_200x200.png?1572879690963" width="159" height="57">
										<div class="nbt-image-banner-info"></div>
									</div>
								</div>
							</aside>
							<aside id="wpnetbase_contact_info_widget-5"
								class="widget widget_wpnetbase_contact_info_widget">
								<div class="contact-info">
									<ul class="info">
										<li><i class="fa fa-home"></i> <span class="street1">Hà
												Nội</span><span class="city-info"></span></li>
										<li><i class="fa fa-phone"></i> <span class="phone">+0123456789</span>
										</li>
										<li><i class="fa fa-envelope-o"></i> <span class="email"><a
												href="" title="send mail"></a>ACC@gmail.com</span></li>
									</ul>


								</div>
							</aside>
							<aside id="wpnetbase_social_media_widget-14"
								class="widget widget_wpnetbase_social_media_widget">
								<div class="sfsi_widget">
									<a
										class="nbt-social-media-icon social-media-icon-facebook social-media-icon-medium"
										href="http://facebook.com" title="Print Shop Facebook"
										target="_blank"><span class="fa fa-facebook"></span></a> <a
										class="nbt-social-media-icon social-media-icon-twitter social-media-icon-medium"
										href="http://twitter.com" title="Print Shop Twitter"
										target="_blank"><span class="fa fa-twitter"></span></a> <a
										class="nbt-social-media-icon social-media-icon-google-plus social-media-icon-medium"
										href="http://plus.google.com" title="Print Shop Google Plus"
										target="_blank"><span class="fa fa-google-plus"></span></a>
								</div>
							</aside>
						</div>
						<div id="footer-2" class="footer-2 footer-column widget-area"
							role="complementary">
							<aside id="nav_menu-8" class="widget widget_nav_menu">
								<h3 class="widget-title">DANH MỤC</h3>
								<div class="menu-footer-menu-1-container">
									<ul id="menu-footer-menu-1" class="menu">
										<li id="menu-item-2930"
											class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2930"><a
											href="#">Tin Tức</a></li>
										<li id="menu-item-2931"
											class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2931"><a
											href="#">Giới Thiệu</a></li>
										<li id="menu-item-2932"
											class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2932"><a
											href="#">Sale</a></li>
										<li id="menu-item-2936"
											class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2936"><a
											href="#">Trang Chủ</a></li>
										<li id="menu-item-2937"
											class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2937"><a
											href="#">Thông Tin</a></li>
										<li id="menu-item-2937"
											class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2937"><a
											href="#">Dịch vụ</a></li>


									</ul>
								</div>
							</aside>
						</div>
						<div id="footer-3" class="footer-3 footer-column widget-area"
							role="complementary">
							<aside id="nav_menu-9" class="widget widget_nav_menu">
								<h3 class="widget-title">Địa chỉ</h3>
								<div class="menu-footer-menu-2-container">
									<ul id="menu-footer-menu-2" class="menu">


										<li
											class="menu-item menu-item-type-custom menu-item-object-custom"><a
											href="#">8a Tôn Thất Thuyết </a></li>

										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.0951822985944!2d105.77955771440736!3d21.02887719315135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313454b3260b1a8b%3A0x862052392e3f478e!2zOCBUw7RuIFRo4bqldCBUaHV54bq_dCwgTeG7uSDEkMOsbmgsIFThu6sgTGnDqm0sIEjDoCBO4buZaSAxMDAwMCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1573024137687!5m2!1svi!2s" width="230" height="150" frameborder="0" style="border: 0;"></iframe>

									</ul>
								</div>
							</aside>
						</div>
						<div id="footer-4" class="footer-4 footer-column widget-area"
							role="complementary">
							<aside id="text-17" class="widget widget_text">
								<h3 class="widget-title">Về chúng tôi</h3>
								<div class="textwidget">Chúng tôi làm việc một cách chuyên
									nghiệp,uy tín,nhanh chóng và luôn đặt quyền lợi của bạn lên
									hàng đầu. AccGame chuyên mua bán nick các game...an toàn, tin
									cậy, nhanh chóng.Giao dịch tự động 24/24.</div>
							</aside>
						</div>
					</div>
				</div>
			</div>
			<div class="site-info-wrapper">
				<div class="container">

					<div
						class="copy_text col-md-10 padding-left-0 padding-right-0 col-xs-12">
						ACC - Copyright © 2019 <a href="http://www.netbaseteam.com/">
							ACC.com</a>
					</div>

				</div>
			</div>
		</footer>
	</div>
</body>
</html>